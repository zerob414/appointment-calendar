# Appointment Calendar

This is my project for the Java Software Development 2 class at Western Governor's University.

## Requirements

School policies do not allow me to upload the rubric, scenario or specific project requirements here, but I will paraphrase as best I can to relay the information.

I was required to write a desktop application for sales employees to schedule appointments with customers. The app was to connect to a MySQL database provided by the school for data persistance. Here are some of the general requirements:
- A login form that would provide feedback to the user when a wrong username or password was entered.
- Internationalization into one language in addition to Enlgish (I chose German).
- Full capabilities to create, edit and delete customer records.
- Full capabilities to create, edit and delete appointments on the calendar.
- A month and week view of the calendar (I also included a day view).
- A log of user logins
- The ability to generate reports.
- The ability to change the timezone the calendar is viewed in.

## Development

This project was developed over the course of about three months during my spare time. It was developed using NetBeans (the IDE the school requires). The interface is JavaFX written in FXML without external CSS files. Minimal inline CSS is used on occasion. During development I discovered the provided databse did not have autoincrementing ID columns and the field used to store user passwords was only 50 characters wide. I received permission from the course instructor to modify the database to make the ID columns autoincrement and to increase the width of the password field to 255 characters to allow proper password salting and hashing with SHA-256. No use of external frameworks were allowed and no more than 30% of the code could be unoriginal, therefore I wrote my own _object relational model (ORM)_ for interacting with database objects. 

## Shortcomings

There are several shortcomings I have identified in the project, some due to constraints applied by the scenario or rubric and others due to the limited amount of time I had to develop the application. Here are the main ones:
- The database security posture is never explicitely defined, so there is no guarantee the connection is using the highest possible encryption level.
- My custom build _object relational model (ORM)_ became a bit unwieldy and difficult to manage once it passed around 700 lines of code. I'm sure there's a better way to do it than what I did.
- The _week_ and _day_ calendar views and separated into their own classes with their own FXML UI trees. The _month_ view, however, is part of the main FXML UI tree. The initial design intention was to have all three be part of the main UI, but when creating the _day_ view it became clear that separting them would be better. If I'd had more time, I would have gone back and separated the _month_ view into it's own class and FXML UI tree.
- In JavaFX multiline text areas, the _tab_ key inserts a _tab_ character rather than moving to the next UI element. This is desirable in some cases, but in my case I wanted the _tab_ key to move to the next UI element. JavaFX provides no built in way to change this behavior. I ended up binding to the key events of the text areas and then walking down the UI tree to find the next (or previous for _shift+tab_) focusable element. It's a bit hacky and breaks if the text area is the first or last element inside a container. Another solution I found on StackOverflow was to bind an event to a utility class deep within JavaFX and override the default behavior for the _tab_ key, however, there were many comments advising against this approach as it messes with JavaFX's internal workings and causes stability issues.