package dbobjects;

import appointmentcalendar.DatabaseManager;
import java.sql.Timestamp;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

/**
 * ORM object for the appointment table.
 * @author Eric Boring <eboring@wgu.edu>
 */
public class Appointment {
    private int appointmentId = -1;
    private Customer customer;
    private User user;
    private String title;
    private String description;
    private String location;
    private String contact;
    private String type;
    private String url;
    private LocalDateTime start;
    private LocalDateTime end;
    private LocalDateTime createDate;
    private String createdBy;
    private LocalDateTime lastUpdate;
    private String lastUpdateBy;
    private boolean dirty = false;
    private DatabaseManager databaseManager;
    private ResourceBundle lang;

    /**
     *
     * @return
     */
    public Appointment save(){
        User loggedInUser = this.getDatabaseManager().getLoggedInUser();
        if(createdBy == null || createdBy == ""){
            createdBy = loggedInUser.getUserName();
            createDate = LocalDateTime.now();
        }
        lastUpdateBy = loggedInUser.getUserName();
        lastUpdate = LocalDateTime.now();
        databaseManager.saveAppointment(this);
        return this;
    }
    
    /**
     *
     * @return
     */
    public Appointment delete(){
        this.getDatabaseManager().deleteAppointment(this);
        return this;
    }
    
    /**
     *
     * @return
     */
    public int getAppointmentId() {
        return appointmentId;
    }

    /**
     *
     * @param appointmentId
     * @return
     */
    public Appointment setAppointmentId(int appointmentId) {
        this.appointmentId = appointmentId;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public Customer getCustomer() {
        return customer;
    }

    /**
     *
     * @param customer
     * @return
     */
    public Appointment setCustomer(Customer customer) {
        this.customer = customer;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public User getUser() {
        return user;
    }

    /**
     *
     * @param user
     * @return
     */
    public Appointment setUser(User user) {
        this.user = user;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public String getTitle() {
        return title;
    }

    /**
     *
     * @param title
     * @return
     */
    public Appointment setTitle(String title) {
        this.title = title;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * @return
     */
    public Appointment setDescription(String description) {
        this.description = description;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public String getLocation() {
        return location;
    }

    /**
     *
     * @param location
     * @return
     */
    public Appointment setLocation(String location) {
        this.location = location;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public String getContact() {
        return contact;
    }

    /**
     *
     * @param contact
     * @return
     */
    public Appointment setContact(String contact) {
        this.contact = contact;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param type
     * @return
     */
    public Appointment setType(String type) {
        this.type = type;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public String getUrl() {
        return url;
    }

    /**
     *
     * @param url
     * @return
     */
    public Appointment setUrl(String url) {
        this.url = url;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public LocalDateTime getStart() {
        return start;
    }

    /**
     *
     * @param start
     * @return
     */
    public Appointment setStart(LocalDateTime start) {
        this.start = start;
        setDirty(true);
        return this;
    }
    
    /**
     *
     * @param start
     * @return
     */
    public Appointment setStart(Timestamp start){
        this.start = start.toLocalDateTime();
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public LocalDateTime getEnd() {
        return end;
    }

    /**
     *
     * @param end
     * @return
     */
    public Appointment setEnd(LocalDateTime end) {
        this.end = end;
        setDirty(true);
        return this;
    }

    /**
     *
     * @param end
     * @return
     */
    public Appointment setEnd(Timestamp end){
        this.end = end.toLocalDateTime();
        setDirty(true);
        return this;
    }
    
    /**
     *
     * @return
     */
    public Duration getDuration(){
        return Duration.between(start, end);
    }
    
    /**
     *
     * @return
     */
    public String getDurationString(){
        Duration d = this.getDuration();
        if(d.getSeconds() < 60){
            return " " + lang == null ? "Less than a minute" : lang.getString("lessThanAMinute");
        }
        else if(d.toMinutes() < 60){
            return d.toMinutes() + " " +(lang == null ? "minutes" : lang.getString("minutes"));
        }
        else if(d.toHours() < 24){
            long remainingMinutes = d.minusHours(d.toHours()).toMinutes();
            if(remainingMinutes == 0){
                return d.toHours() + " "  + (lang == null ? "hours" : lang.getString("hours"));
            }
            else{
                return d.toHours() + " "  + (lang == null ? "hours, "  : lang.getString("hours")) + " "  + remainingMinutes + lang == null ? "minutes" : lang.getString("minutes");
            }
        }
        else{
            Duration remHours = d.minusDays(d.toDays());
            long remainingHours = remHours.toHours();
            long remainingMinutes = remHours.minusHours(remHours.toHours()).toMinutes();
            String str = d.toDays() + " "  + (lang == null ? "days" : lang.getString("days"));
            if(remainingHours > 0){
                str += ", " + remainingHours + lang == null ? "hours" : lang.getString("hours");
            }
            if(remainingMinutes > 0){
                str += ", " + remainingMinutes + lang == null ? "minutes" : lang.getString("minutes");
            }
            return str;
        }
    }
    
    /**
     *
     * @return
     */
    public LocalDateTime getCreateDate() {
        return createDate;
    }

    /**
     *
     * @param createDate
     * @return
     */
    public Appointment setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
        setDirty(true);
        return this;
    }
    
    /**
     *
     * @param createDate
     * @return
     */
    public Appointment setCreateDate(Timestamp createDate){
        this.createDate = createDate.toLocalDateTime();
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     *
     * @param createdBy
     * @return
     */
    public Appointment setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    /**
     *
     * @param lastUpdate
     * @return
     */
    public Appointment setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
        setDirty(true);
        return this;
    }
    
    /**
     *
     * @param lastUpdate
     * @return
     */
    public Appointment setLastUpdate(Timestamp lastUpdate){
        this.lastUpdate = lastUpdate.toLocalDateTime();
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public String getLastUpdateBy() {
        return lastUpdateBy;
    }

    /**
     *
     * @param lastUpdateBy
     * @return
     */
    public Appointment setLastUpdateBy(String lastUpdateBy) {
        this.lastUpdateBy = lastUpdateBy;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public boolean isDirty() {
        return dirty;
    }

    /**
     *
     * @param dirty
     * @return
     */
    public Appointment setDirty(boolean dirty) {
        this.dirty = dirty;
        return this;
    }

    /**
     *
     * @return
     */
    public DatabaseManager getDatabaseManager() {
        return databaseManager;
    }

    /**
     *
     * @param databaseManager
     * @return
     */
    public Appointment setDatabaseManager(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
        return this;
    }

    public ResourceBundle getLang() {
        return lang;
    }

    public Appointment setLang(ResourceBundle lang) {
        this.lang = lang;
        return this;
    }
    
    @Override
    public boolean equals(Object other){
        if(other instanceof Appointment){
            Appointment appt = (Appointment) other;
            if(appt.getAppointmentId() != this.appointmentId)return false;
            if(!appt.getContact().equals(this.contact))return false;            
            if(!appt.getCreateDate().equals(this.createDate))return false;
            if(!appt.getCreatedBy().equals(this.createdBy))return false;
            if(appt.getCustomer() == null && this.customer != null || appt.getCustomer() != null && this.customer == null)return false;
            else if(!appt.getCustomer().equals(this.customer))return false;
            if(!appt.getDescription().equals(this.description))return false;
            if(!appt.getEnd().equals(this.end))return false;
            if(!appt.getLastUpdate().equals(this.lastUpdate))return false;
            if(!appt.getLastUpdateBy().equals(this.lastUpdateBy))return false;
            if(!appt.getLocation().equals(this.location))return false;
            if(!appt.getStart().equals(this.start))return false;
            if(!appt.getTitle().equals(this.title))return false;
            if(!appt.getType().equals(this.type))return false;
            if(!appt.getUrl().equals(this.url))return false;
            if(appt.getUser() == null && this.user != null || appt.getUser() != null && this.user == null)return false;
            else if(!appt.getUser().equals(this.user))return false;
            return true;
        }
        else{
            return false;
        }
    }
}
