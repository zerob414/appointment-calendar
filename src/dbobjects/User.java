package dbobjects;

import appointmentcalendar.DatabaseManager;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * ORM object for the user table.
 * @author Eric Boring <eboring@wgu.edu>
 */
public class User {
    private int userId = -1;
    private String userName = "";
    private Password password;
    private boolean active;
    private LocalDateTime createDate;
    private String createdBy;
    private LocalDateTime lastUpdate;
    private String lastUpdateBy;
    private DatabaseManager databaseManager;
    private boolean dirty;
    
    /**
     *
     * @return
     */
    public User save(){
        User user = this.getDatabaseManager().getLoggedInUser();
        if(createdBy == null || createdBy == ""){
            createdBy = user.getUserName();
            createDate = LocalDateTime.now();
        }
        lastUpdateBy = user == null ? "" : user.getUserName();
        lastUpdate = LocalDateTime.now();
        return databaseManager.saveUser(this);
    }

    /**
     *
     * @return
     */
    public int getUserId() {
        return userId;
    }

    /**
     *
     * @param userId
     * @return
     */
    public User setUserId(int userId) {
        this.userId = userId;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public String getUserName() {
        return userName;
    }

    /**
     *
     * @param userName
     * @return
     */
    public User setUserName(String userName) {
        this.userName = userName;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public Password getPassword() {
        return password;
    }

    /**
     *
     * @param password
     * @return
     */
    public User setPassword(Password password) {
        this.password = password;
        return this;
    }
    
    /**
     *
     * @param password
     * @return
     */
    public User setPassword(byte[] password) {
        setPassword(password);
        return this;
    }

    /**
     *
     * @return
     */
    public boolean isActive() {
        return active;
    }

    /**
     *
     * @param active
     * @return
     */
    public User setActive(boolean active) {
        this.active = active;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public LocalDateTime getCreateDate() {
        return createDate;
    }

    /**
     *
     * @param createDate
     * @return
     */
    public User setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
        setDirty(true);
        return this;
    }
    
    /**
     *
     * @param createDate
     * @return
     */
    public User setCreateDate(Timestamp createDate){
        setCreateDate(createDate.toLocalDateTime());
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     *
     * @param createdBy
     * @return
     */
    public User setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    /**
     *
     * @param lastUpdate
     * @return
     */
    public User setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
        setDirty(true);
        return this;
    }

    /**
     *
     * @param lastUpdate
     * @return
     */
    public User setLastUpdate(Timestamp lastUpdate){
        setLastUpdate(lastUpdate.toLocalDateTime());
        setDirty(true);
        return this;
    }
    
    /**
     *
     * @return
     */
    public String getLastUpdateBy() {
        return lastUpdateBy;
    }

    /**
     *
     * @param lastUpdateBy
     * @return
     */
    public User setLastUpdateBy(String lastUpdateBy) {
        this.lastUpdateBy = lastUpdateBy;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public DatabaseManager getDatabaseManager() {
        return databaseManager;
    }

    /**
     *
     * @param databaseManager
     * @return
     */
    public User setDatabaseManager(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
        return this;
    }

    /**
     *
     * @return
     */
    public boolean isDirty() {
        return dirty;
    }

    /**
     *
     * @param dirty
     * @return
     */
    public User setDirty(boolean dirty) {
        this.dirty = dirty;
        return this;
    }
    
    @Override
    public boolean equals(Object other){
        if(other instanceof User){
            User user = (User)other;
            if(user.isActive() != this.active)return false;
            if(!user.getCreateDate().equals(this.createDate))return false;
            if(!user.getCreatedBy().equals(this.createdBy))return false;
            if(!user.getLastUpdate().equals(this.lastUpdate))return false;
            if(!user.getLastUpdateBy().equals(this.lastUpdateBy))return false;
            if(user.getPassword() == null && this.password != null || user.getPassword() != null && this.password == null)return false;
            else if(!user.getPassword().equals(this.password))return false;
            if(user.getUserId() != this.userId)return false;
            if(!user.getUserName().equals(this.userName))return false;
            return true;
        }
        else{
            return false;
        }
    }
}
