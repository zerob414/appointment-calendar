package dbobjects;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.DatatypeConverter;

/**
 * ORM object to abstract managing password hashing and salting for the user table.
 * @author Eric Boring <eboring@wgu.edu>
 */
public class Password {

    public static final String MD5 = "MD5";
    public static final String SHA_1 = "SHA-1";
    public static final String SHA_224 = "SHA-224";
    public static final String SHA_256 = "SHA-256";
    public static final String SHA_384 = "SHA-384";
    public static final String SHA_512 = "SHA-512";
    public static final int DEFAULT_SALT_LENGTH = 16;
    
    private static final SecureRandom secureRandom = new SecureRandom();
    
    private byte[] password;
    private byte[] salt;
    private String algorithm = SHA_256;
    
    /**
     * Hashes the supplied password with the supplied salt using SHA-256.
     * @param password
     * @param salt
     * @return
     */
    public static byte[] hashPassword(byte[] password, byte[] salt){
        return hashPassword(password, salt, SHA_256);
    }
    
    /**
     * Hashes the supplied password with the supplied salt using the specified algorithm.
     * @param password
     * @param salt
     * @param algorithm
     * @return
     */
    public static byte[] hashPassword(byte[] password, byte[] salt, String algorithm){
        byte[] saltedPassword = Password.concatByteArrays(password, salt);
        byte[] hashedPassword = null;
        try {
            MessageDigest hasher = MessageDigest.getInstance(algorithm);
            hashedPassword = hasher.digest(saltedPassword);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Password.class.getName()).log(Level.SEVERE, null, ex);
        }
        return hashedPassword;
    }
    
    /**
     * Uses SecureRandom to generate a salt byte array of the specified length.
     * @param length
     * @return
     */
    public static byte[] generateSalt(int length){
        byte[] salt = new byte[length];
        secureRandom.nextBytes(salt);
        return salt;
    }
    
    /**
     * 
     * @param input
     * @return
     */
    public static String convertByteArrayToHexString(byte[] input){
        return DatatypeConverter.printHexBinary(input);
    }
    
    /**
     *
     * @param input
     * @return
     */
    public static byte[] convertHexStringToByteArray(String input){
        return DatatypeConverter.parseHexBinary(input);
    }
    
    /**
     * Concatenates two byte arrays.
     * @param a
     * @param b
     * @return
     */
    public static byte[] concatByteArrays(byte[] a, byte[] b){
        byte[] c = new byte[a.length + b.length];
        System.arraycopy(a, 0, c, 0, a.length);
        System.arraycopy(b, 0, c, a.length, b.length);
        return c;
    }
    
    @Override
    public boolean equals(Object obj){
        if(obj instanceof Password){
            Password p = (Password)obj;
            return p.getPassword() == this.getPassword() && p.getSalt() == this.getSalt();
        }
        else{
            return false;
        }
    }
    
    /**
     * Checks if the password matches this password when hashed with this password's salt.
     * @param plainTextPassword
     * @param algorithm
     * @return
     */
    public boolean isPassword(String plainTextPassword, String algorithm){
        byte[] hashedPassword = hashPassword(plainTextPassword.getBytes(), this.salt, algorithm);
        return Arrays.equals(hashedPassword, this.password);
    }
    
    /**
     * Checks if the password matches this password when hashed with this password's salt.
     * @param plainTextPassword
     * @return
     */
    public boolean isPassword(String plainTextPassword){
        return isPassword(plainTextPassword, this.algorithm);
    }
    
    /**
     * Returns this password as a byte array.
     * @return
     */
    public byte[] getPassword() {
        return password;
    }
    
    /**
     * Returns this password as a hex string.
     * @return
     */
    public String getPasswordHex(){
        return convertByteArrayToHexString(getPassword());
    }

    /**
     * Sets this password. The byte array must be already hashed.
     * @param password
     * @return
     */
    public Password setPassword(byte[] password) {
        this.password = password;
        return this;
    }
    
    /**
     * Sets this password by hashing the passed in string.
     * @param password
     * @return
     */
    public Password setPasswordPlainText(String password){
        this.setPassword(hashPassword(password.getBytes(), this.getSalt()));
        return this;
    }
    
    /**
     * Sets this password and salt. The password will be hashed automatically.
     * @param password
     * @param salt
     * @return
     */
    public Password setPasswordPlainText(String password, byte[] salt){
        this.setSalt(salt);
        this.setPasswordPlainText(password);
        return this;
    }
    
    /**
     * Sets this password from the hex string. The password should have been
     * hashed before converted to hex.
     * @param password
     * @return
     */
    public Password setPasswordHex(String password){
        this.setPassword(convertHexStringToByteArray(password));
        return this;
    }
    
    /**
     * Sets this password from the hex string. The password should have been
     * hashed before converted to hex. This typically comes from a database
     * password column.
     * @param combo
     * @return
     */
    public Password setPasswordAndSaltHex(String combo){
        String[] parts = combo.split(":");
        setPasswordHex(parts[0]);
        setSaltHex(parts[1]);
        return this;
    }
    
    /**
     * Returns the hex password and salt combination for storage in a database
     * password column.
     * @return
     */
    public String getPasswordAndSaltHex(){
        return this.getPasswordHex() + ":" + this.getSaltHex();
    }

    /**
     *
     * @return
     */
    public byte[] getSalt() {
        return salt;
    }

    /**
     *
     * @param salt
     * @return
     */
    public Password setSalt(byte[] salt) {
        this.salt = salt;
        return this;
    }

    /**
     *
     * @return
     */
    public String getSaltHex(){
        return convertByteArrayToHexString(getSalt());
    }
    
    /**
     *
     * @param salt
     * @return
     */
    public Password setSaltHex(String salt){
        this.setSalt(convertHexStringToByteArray(salt));
        return this;
    }

    /**
     *
     * @return
     */
    public String getAlgorithm() {
        return algorithm;
    }

    /**
     *
     * @param algorithm
     * @return
     */
    public Password setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
        return this;
    }
    
}
