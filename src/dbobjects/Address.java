package dbobjects;

import appointmentcalendar.DatabaseManager;
import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * ORM object for the address table.
 * @author Eric Boring <eboring@wgu.edu>
 */
public class Address {
    private int addressId = -1;
    private String address = "";
    private String address2 = "";
    private City city;
    private String postalCode = "";
    private String phone = "";
    private LocalDateTime createDate;
    private String createdBy;
    private LocalDateTime lastUpdate;
    private String lastUpdateBy;
    private DatabaseManager databaseManager;
    private boolean dirty = false;

    /**
     *
     * @return
     */
    public int getAddressId() {
        return addressId;
    }

    /**
     *
     * @param addressId
     * @return
     */
    public Address setAddressId(int addressId) {
        this.addressId = addressId;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public String getAddress() {
        return address;
    }

    /**
     *
     * @param address
     * @return
     */
    public Address setAddress(String address) {
        this.address = address;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public String getAddress2() {
        return address2;
    }

    /**
     *
     * @param address2
     * @return
     */
    public Address setAddress2(String address2) {
        this.address2 = address2;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public City getCity() {
        return city;
    }

    /**
     *
     * @param city
     * @return
     */
    public Address setCity(City city) {
        this.city = city;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     *
     * @param postalCode
     * @return
     */
    public Address setPostalCode(String postalCode) {
        this.postalCode = postalCode;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public String getPhone() {
        return phone;
    }

    /**
     *
     * @param phone
     * @return
     */
    public Address setPhone(String phone) {
        this.phone = phone;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public LocalDateTime getCreateDate() {
        return createDate;
    }

    /**
     *
     * @param createDate
     * @return
     */
    public Address setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
        setDirty(true);
        return this;
    }
    
    /**
     *
     * @param createDate
     * @return
     */
    public Address setCreateDate(Timestamp createDate) {
        setCreateDate(createDate.toLocalDateTime());
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     *
     * @param createdBy
     * @return
     */
    public Address setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    /**
     *
     * @param lastUpdate
     * @return
     */
    public Address setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
        setDirty(true);
        return this;
    }
    
    /**
     *
     * @param lastUpdate
     * @return
     */
    public Address setLastUpdate(Timestamp lastUpdate) {
        setLastUpdate(lastUpdate.toLocalDateTime());
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public String getLastUpdateBy() {
        return lastUpdateBy;
    }

    /**
     *
     * @param lastUpdateBy
     * @return
     */
    public Address setLastUpdateBy(String lastUpdateBy) {
        this.lastUpdateBy = lastUpdateBy;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public DatabaseManager getDatabaseManager() {
        return databaseManager;
    }

    /**
     *
     * @param databaseManager
     * @return
     */
    public Address setDatabaseManager(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
        return this;
    }

    /**
     *
     * @return
     */
    public boolean isDirty() {
        return dirty;
    }

    /**
     *
     * @param dirty
     * @return
     */
    public Address setDirty(boolean dirty) {
        this.dirty = dirty;
        return this;
    }
    
    /**
     *
     * @return
     */
    public Address save(){
        User user = this.getDatabaseManager().getLoggedInUser();
        if(createdBy == null || createdBy == ""){
            createdBy = user.getUserName();
            createDate = LocalDateTime.now();
        }
        lastUpdateBy = user.getUserName();
        lastUpdate = LocalDateTime.now();
        databaseManager.saveAddress(this);
        return this;
    }
    
    /**
     *
     * @return
     */
    public Address delete(){
        this.getDatabaseManager().deleteAddess(this);
        return this;
    }
    
    @Override
    public boolean equals(Object other){
        if(other instanceof Address){
            Address addr = (Address)other;
            if(!addr.getAddress().equals(this.address))return false;
            if(!addr.getAddress2().equals(this.address2))return false;
            if(addr.getAddressId() != this.addressId)return false;
            if(addr.getCity() == null && this.city != null || addr.getCity() != null && this.city == null)return false;
            else if(!addr.getCity().equals(this.city))return false;
            if(!addr.getCreateDate().equals(this.createDate))return false;
            if(!addr.getCreatedBy().equals(this.createdBy))return false;
            if(!addr.getLastUpdate().equals(this.lastUpdate))return false;
            if(!addr.getLastUpdateBy().equals(this.lastUpdateBy))return false;
            if(!addr.getPhone().equals(this.phone))return false;
            if(!addr.getPostalCode().equals(this.postalCode))return false;
            return true;
        }
        else{
            return false;
        }
    }
}
