package dbobjects;

import appointmentcalendar.DatabaseManager;
import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * ORM object for the city table.
 * @author Eric Boring <eboring@wgu.edu>
 */
public class City {
    private int cityId = -1;
    private String city = "";
    private Country country;
    private LocalDateTime createDate;
    private String createdBy;
    private LocalDateTime lastUpdate;
    private String lastUpdateBy;
    private DatabaseManager databaseManager;
    private boolean dirty = false;

    /**
     *
     * @return
     */
    public int getCityId() {
        return cityId;
    }

    /**
     *
     * @param cityId
     * @return
     */
    public City setCityId(int cityId) {
        this.cityId = cityId;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public String getCity() {
        return city;
    }

    /**
     *
     * @param city
     * @return
     */
    public City setCity(String city) {
        this.city = city;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public Country getCountry() {
        return country;
    }

    /**
     *
     * @param country
     * @return
     */
    public City setCountry(Country country) {
        this.country = country;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public LocalDateTime getCreateDate() {
        return createDate;
    }

    /**
     *
     * @param createDate
     * @return
     */
    public City setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
        setDirty(true);
        return this;
    }
    
    /**
     *
     * @param createDate
     * @return
     */
    public City setCreateDate(Timestamp createDate) {
        setCreateDate(createDate.toLocalDateTime());
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     *
     * @param createdBy
     * @return
     */
    public City setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    /**
     *
     * @param lastUpdate
     * @return
     */
    public City setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
        setDirty(true);
        return this;
    }
    
    /**
     *
     * @param lastUpdate
     * @return
     */
    public City setLastUpdate(Timestamp lastUpdate) {
        setLastUpdate(lastUpdate.toLocalDateTime());
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public String getLastUpdateBy() {
        return lastUpdateBy;
    }

    /**
     *
     * @param lastUpdateBy
     * @return
     */
    public City setLastUpdateBy(String lastUpdateBy) {
        this.lastUpdateBy = lastUpdateBy;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public DatabaseManager getDatabaseManager() {
        return databaseManager;
    }

    /**
     *
     * @param databaseManager
     * @return
     */
    public City setDatabaseManager(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
        return this;
    }

    /**
     *
     * @return
     */
    public boolean isDirty() {
        return dirty;
    }

    /**
     *
     * @param dirty
     * @return
     */
    public City setDirty(boolean dirty) {
        this.dirty = dirty;
        return this;
    }

    /**
     *
     * @return
     */
    public City save() {
        User user = this.getDatabaseManager().getLoggedInUser();
        if(createdBy == null || createdBy == ""){
            createdBy = user.getUserName();
            createDate = LocalDateTime.now();
        }
        lastUpdateBy = user.getUserName();
        lastUpdate = LocalDateTime.now();
        databaseManager.saveCity(this);
        return this;
    }
    
    @Override
    public String toString(){
        return getCity();
    }
    
    @Override
    public boolean equals(Object other){
        if(other instanceof City){
            City city = (City)other;
            if(!city.getCity().equals(this.city))return false;
            if(city.getCityId() != this.cityId)return false;
            if(city.getCountry() == null && this.country != null || city.getCountry() != null && this.country == null)return false;
            else if(!city.getCountry().equals(this.country))return false;
            if(!city.getCreateDate().equals(this.createDate))return false;
            if(!city.getCreatedBy().equals(this.createdBy))return false;
            if(!city.getLastUpdate().equals(this.lastUpdate))return false;
            if(!city.getLastUpdateBy().equals(this.lastUpdateBy))return false;
            return true;
        }
        else{
            return false;
        }
    }
}
