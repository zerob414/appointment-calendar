package dbobjects;

import appointmentcalendar.DatabaseManager;
import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * ORM object for the country table.
 * @author Eric Boring <eboring@wgu.edu>
 */
public class Country {
    private int countryId = -1;
    private String country = "";
    private LocalDateTime createDate;
    private String createdBy;
    private LocalDateTime lastUpdate;
    private String lastUpdateBy;
    private DatabaseManager databaseManager;
    private boolean dirty = false;

    /**
     *
     * @return
     */
    public int getCountryId() {
        return countryId;
    }

    /**
     *
     * @param countryId
     * @return
     */
    public Country setCountryId(int countryId) {
        this.countryId = countryId;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public String getCountry() {
        return country;
    }

    /**
     *
     * @param country
     * @return
     */
    public Country setCountry(String country) {
        this.country = country;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public LocalDateTime getCreateDate() {
        return createDate;
    }

    /**
     *
     * @param createDate
     * @return
     */
    public Country setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
        setDirty(true);
        return this;
    }

    /**
     *
     * @param createDate
     * @return
     */
    public Country setCreateDate(Timestamp createDate) {
        setCreateDate(createDate.toLocalDateTime());
        setDirty(true);
        return this;
    }
    
    /**
     *
     * @return
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     *
     * @param createdBy
     * @return
     */
    public Country setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    /**
     *
     * @param lastUpdate
     * @return
     */
    public Country setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
        setDirty(true);
        return this;
    }
    
    /**
     *
     * @param lastUpdate
     * @return
     */
    public Country setLastUpdate(Timestamp lastUpdate) {
        setLastUpdate(lastUpdate.toLocalDateTime());
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public String getLastUpdateBy() {
        return lastUpdateBy;
    }

    /**
     *
     * @param lastUpdateBy
     * @return
     */
    public Country setLastUpdateBy(String lastUpdateBy) {
        this.lastUpdateBy = lastUpdateBy;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public DatabaseManager getDatabaseManager() {
        return databaseManager;
    }

    /**
     *
     * @param databaseManager
     * @return
     */
    public Country setDatabaseManager(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
        return this;
    }

    /**
     *
     * @return
     */
    public boolean isDirty() {
        return dirty;
    }

    /**
     *
     * @param dirty
     * @return
     */
    public Country setDirty(boolean dirty) {
        this.dirty = dirty;
        return this;
    }

    /**
     *
     * @return
     */
    public Country save() {
        User user = this.getDatabaseManager().getLoggedInUser();
        if(createdBy == null || createdBy == ""){
            createdBy = user.getUserName();
            createDate = LocalDateTime.now();
        }
        lastUpdateBy = user.getUserName();
        lastUpdate = LocalDateTime.now();
        databaseManager.saveCountry(this);
        return this;
    }
    
    @Override
    public String toString(){
        return getCountry();
    }
    
    @Override
    public boolean equals(Object other){
        if(other instanceof Country){
            Country cnty = (Country)other;
            if(!cnty.getCountry().equals(this.country))return false;
            if(cnty.getCountryId() != this.countryId)return false;
            if(!cnty.getCreateDate().equals(this.createDate))return false;
            if(!cnty.getCreatedBy().equals(this.createdBy))return false;
            if(!cnty.getLastUpdate().equals(this.lastUpdate))return false;
            if(!cnty.getLastUpdateBy().equals(this.lastUpdateBy))return false;
            return true;
        }
        else{
            return false;
        }
    }
}
