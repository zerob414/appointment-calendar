package dbobjects;

import appointmentcalendar.DatabaseManager;
import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * ORM object for the customer table.
 * @author Eric Boring <eboring@wgu.edu>
 */
public class Customer {
    private int customerId = -1;
    private String customerName = "";
    private Address address;
    private boolean active = true;
    private LocalDateTime createDate;
    private String createdBy;
    private LocalDateTime lastUpdate;
    private String lastUpdateBy;
    private DatabaseManager databaseManager;
    private boolean dirty = false;

    /**
     *
     * @return
     */
    public int getCustomerId() {
        return customerId;
    }

    /**
     *
     * @param customerId
     * @return
     */
    public Customer setCustomerId(int customerId) {
        this.customerId = customerId;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     *
     * @param customerName
     * @return
     */
    public Customer setCustomerName(String customerName) {
        this.customerName = customerName;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public Address getAddress() {
        return address;
    }

    /**
     *
     * @param address
     * @return
     */
    public Customer setAddress(Address address) {
        this.address = address;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public boolean isActive() {
        return active;
    }

    /**
     *
     * @param active
     * @return
     */
    public Customer setActive(boolean active) {
        this.active = active;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public LocalDateTime getCreateDate() {
        return createDate;
    }

    /**
     *
     * @param createDate
     * @return
     */
    public Customer setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
        setDirty(true);
        return this;
    }
    
    /**
     *
     * @param createDate
     * @return
     */
    public Customer setCreateDate(Timestamp createDate) {
        setCreateDate(createDate.toLocalDateTime());
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     *
     * @param createdBy
     * @return
     */
    public Customer setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    /**
     *
     * @param lastUpdate
     * @return
     */
    public Customer setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
        setDirty(true);
        return this;
    }
    
    /**
     *
     * @param lastUpdate
     * @return
     */
    public Customer setLastUpdate(Timestamp lastUpdate) {
        setLastUpdate(lastUpdate.toLocalDateTime());
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public String getLastUpdateBy() {
        return lastUpdateBy;
    }

    /**
     *
     * @param lastUpdateBy
     * @return
     */
    public Customer setLastUpdateBy(String lastUpdateBy) {
        this.lastUpdateBy = lastUpdateBy;
        setDirty(true);
        return this;
    }

    /**
     *
     * @return
     */
    public DatabaseManager getDatabaseManager() {
        return databaseManager;
    }

    /**
     *
     * @param databaseManager
     * @return
     */
    public Customer setDatabaseManager(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
        return this;
    }

    /**
     *
     * @return
     */
    public boolean isDirty() {
        return dirty;
    }

    /**
     *
     * @param dirty
     * @return
     */
    public Customer setDirty(boolean dirty) {
        this.dirty = dirty;
        return this;
    }
    
    @Override
    public String toString(){
        return getCustomerName();
    }
    
    /**
     *
     * @return
     */
    public Customer save(){
        User user = this.getDatabaseManager().getLoggedInUser();
        if(createdBy == null || createdBy == ""){
            createdBy = user.getUserName();
            createDate = LocalDateTime.now();
        }
        lastUpdateBy = user.getUserName();
        lastUpdate = LocalDateTime.now();
        this.getDatabaseManager().saveCustomer(this);
        return this;
    }
    
    /**
     *
     * @return
     */
    public Customer delete(){
        this.getDatabaseManager().deleteCustomer(this);
        this.customerId = -1;
        return this;
    }
    
    @Override
    public boolean equals(Object other){
        if(other instanceof Customer){
            Customer cust = (Customer)other;
            if(cust.isActive() != this.active)return false;
            if(cust.getAddress() == null && this.address != null || cust.getAddress() != null && this.address == null)return false;
            else if(!cust.getAddress().equals(this.address))return false;
            if(!cust.getCreateDate().equals(this.createDate))return false;
            if(!cust.getCreatedBy().equals(this.createdBy))return false;
            if(cust.getCustomerId() != this.customerId)return false;
            if(!cust.getCustomerName().equals(this.customerName))return false;
            if(!cust.getLastUpdate().equals(this.lastUpdate))return false;
            if(!cust.getLastUpdateBy().equals(this.lastUpdateBy))return false;
            return true;
        }
        else{
            return false;
        }
    }
}
