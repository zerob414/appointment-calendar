package appointmentcalendar;

import dbobjects.Appointment;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

/**
 * FXML Controller class
 * Manages the day view of the application.
 * @author Eric Boring <eboring@wgu.edu>
 */
public class DayViewController implements Initializable {
    private MainWindowController mainWindowController;
    private LocalDate date;
    private Scene scene;
    
    @FXML
    private GridPane grid;

    @FXML
    private Label lblTitle;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        LocalTime time = LocalTime.MIDNIGHT;
        for(int i = 0; i < 48; i += 2){
            Label lblTime = new Label(time.format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT)));
            HBox hb = new HBox();
            hb.setAlignment(Pos.TOP_CENTER);
            hb.getChildren().add(lblTime);
            grid.add(hb, 0, i, 1, 2);
            lblTime.setPadding(new Insets(5.0));
            String color = time.getHour() % 2 == 1 ? "azure;" : "white;";
            hb.setStyle("-fx-background-color: " + color);

            time = time.plusHours(1);
        }
    }    

    /**
     * Returns the date this day view is showing.
     * @return
     */
    public LocalDate getDate() {
        return date;
    }

    /**
     * Sets the date this day view is showing and loads all of the appointments
     * to display.
     * @param date
     */
    public void setDate(LocalDate date) {
        this.date = date;
        lblTitle.setText(date.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)));
        if(this.mainWindowController != null){
            DatabaseManager databaseManager = this.mainWindowController.getAppointmentCalendarApp().getDatabaseManager();
            ArrayList<Appointment> appointments = databaseManager.getAppointmentsOnDate(date);
            clearAppointmentsColumn();
            for(Appointment appt : appointments){
                int start = appt.getStart().getHour() * 2;
                int offset = appt.getStart().getMinute() > 29 ? 1 : 0;
                int vSpan = (int)Math.round(appt.getDuration().toMinutes() / 30.0);
                vSpan = vSpan > 0 ? vSpan : 1;
                AppointmentHyperlink apptLink = new AppointmentHyperlink(appt, AppointmentHyperlink.Size.LARGE);
                apptLink.setOnAction(e -> {
                    AppointmentHyperlink l = (AppointmentHyperlink)e.getSource();
                    l.setStyle("-fx-font-weight: bold; -fx-background-color: yellow;");
                    this.mainWindowController.updateInfoPanel(l.getAppointment(), date);
                    this.mainWindowController.setSelectedAppointment(l.getAppointment());
                });
                FlowPane fp = new FlowPane();
                fp.setStyle("-fx-background-color: #ffcccc; -fx-background-radius: 10;");
                fp.getChildren().add(apptLink);
                grid.add(fp, 1, start + offset, 1, vSpan);
            }
            
            for(int i = 0; i < 48; i++){
                Node node = getNodeFromGridPane(grid, 1, i);
                if(node == null){
                    FlowPane fp = new FlowPane();
                    String color = i / 2  % 2 == 1 ? "azure;" : "white;";
                    fp.setStyle("-fx-background-color: " + color);
                    grid.add(fp, 1, i);
                    fp.toBack();
                }
            }
        }
    }
    
    private void clearAppointmentsColumn(){
        ArrayList<Node> removeList = new ArrayList<>();
        for(Node node : grid.getChildrenUnmodifiable()){
            Integer col = GridPane.getColumnIndex(node);
            col = col == null ? 0 : col;
            if(col > 0){
                removeList.add(node);
            }
        }
        grid.getChildren().removeAll(removeList);
    }
    
    /**
     * Returns the MainWindowController this day view is associated with.
     * @return
     */
    public MainWindowController getMainWindowController() {
        return mainWindowController;
    }

    /**
     * Sets the MainWindowController this day view is associated with.
     * @param mainWindowController
     */
    public void setMainWindowController(MainWindowController mainWindowController) {
        this.mainWindowController = mainWindowController;
    }
    
    /**
     * Returns the node located at the specified coordinates in the GridPane.
     * Borrowed from Shreyas Dave at: https://stackoverflow.com/questions/20655024/javafx-gridpane-retrieve-specific-cell-content
     * @param gridPane
     * @param col
     * @param row
     * @return 
     */
    private Node getNodeFromGridPane(GridPane gridPane, int col, int row) {
        for (Node node : gridPane.getChildren()) {
            if (GridPane.getColumnIndex(node) == col && GridPane.getRowIndex(node) == row) {
                return node;
            }
        }
        return null;
    }

    /**
     * Returns this controller's scene.
     * @return
     */
    public Scene getScene() {
        return scene;
    }

    /**
     * Sets this controller's scene.
     * @param scene
     */
    public void setScene(Scene scene) {
        this.scene = scene;
    }
    
}
