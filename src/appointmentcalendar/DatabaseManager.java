package appointmentcalendar;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import dbobjects.*;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.MessageFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * Provides an object relational model to further abstract persistent data
 * storage away from the main application. Relies on the objects found in the
 * dbobjects package.
 * @author Eric Boring <eboring@wgu.edu>
 */
public class DatabaseManager {
    private String databaseLocation = "52.206.157.109";
    private String databaseName = "U04dYf";
    private String databaseUsername = "U04dYf";
    private String databasePassword = "53688213141";

    private ResourceBundle lang;
    
    /**
     * Determines how results of a query should be sorted.
     */
    public enum Sort {

        /**
         * Do not sort results, preserve the order found in the database.
         */
        NONE, 

        /**
         * Sort results in ascending order.
         */
        ASC, 

        /**
         * Sort results in descending order.
         */
        DESC}
    
    /**
     * Represents the status of the database's connection.
     */
    public enum ConnectionStatus {

        /**
         * The database is connected.
         */
        CONNECTED, 

        /**
         * The database is not connected.
         */
        DISCONNECTED}
    private ConnectionStatus connectionStatus = ConnectionStatus.DISCONNECTED;
    private Connection connection = null;
    
    private User loggedInUser;

    // Connection and Miscellaneous Methods ====================================

    /**
     * Looks up the username in the database and then compares the password
     * to attempt user authentication. If the username and password are a match,
     * the user object is returned, otherwise null is returned.
     * @param username
     * @param password
     * @return
     */
    public User loginUser(String username, String password){
        User user = null;
        if(!isConnected()){
            connect();
        }
        if(isConnected()){
            User candidateUser = getUserByUsername(username);
            if(candidateUser != null){
                if(candidateUser.getPassword().isPassword(password)){
                    user = candidateUser;
                }
            }
        }
        setLoggedInUser(user);
        return user;
    }
    
    /**
     * Attempts to connect to the database. In production, more care should be
     * taken to ensure this is secure. For example, strong TLS encryption may be
     * used or even an SSH public/private key pair. Furthermore, the database
     * provided by uCertify seems to be inconsistent and has a few deficiencies.
     * I am correcting them here, but in a real application these would not be an
     * issue as any competent DBA would already have implemented these things.
     */
    public void connect(){
        try {
            connection = DriverManager.getConnection("jdbc:mysql://" + getDatabaseLocation() + "/" + getDatabaseName(), getDatabaseUsername(),getDatabasePassword());
            if(!connection.isClosed()){
                setConnectionStatus(ConnectionStatus.CONNECTED);
                //I discovered the supplied database lacks auto incrementing ID columns, so let's ensure that is corrected:
                Statement stmt = connection.createStatement();
                stmt.addBatch("ALTER TABLE `address` MODIFY COLUMN `addressId` INT auto_increment;");
                stmt.addBatch("ALTER TABLE `appointment` MODIFY COLUMN `appointmentId` INT auto_increment;");
                stmt.addBatch("ALTER TABLE `city` MODIFY COLUMN `cityId` INT auto_increment;");
                stmt.addBatch("ALTER TABLE `country` MODIFY COLUMN `countryId` INT auto_increment;");
                stmt.addBatch("ALTER TABLE `customer` MODIFY COLUMN `customerId` INT auto_increment;");
                stmt.addBatch("ALTER TABLE `user` MODIFY COLUMN `userId` INT auto_increment;");
                //The password column size is insufficient for proper password storage. Since a real production system would not have this issue...
                stmt.addBatch("ALTER TABLE `user` CHANGE COLUMN `password` `password` VARCHAR(255)");
                stmt.executeBatch();
            }
            
        } catch (SQLException ex) {
            setConnectionStatus(ConnectionStatus.DISCONNECTED);
            String title = lang != null ? lang.getString("apptitle") : "Appointment Calendar";
            String header = lang != null ? lang.getString("error") : "Error";
            String message = lang != null ? MessageFormat.format(lang.getString("errorDatabaseLocation"), getDatabaseLocation(), getDatabaseUsername()) : "Unable to connect to the database at " + getDatabaseLocation() + ", please check that the location is correct, that the database is reachable, and that " + getDatabaseUsername() + " has proper permissions, then try again.";
            DialogPopper.showError(title, header, message , null, null);
            Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Disconnects from the database.
     */
    public void disconnect(){
        try {
            connection.close();
            if(!connection.isClosed()) setConnectionStatus(ConnectionStatus.CONNECTED);
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private boolean isConnected() {
        return getConnectionStatus() == ConnectionStatus.CONNECTED;
    }
    
    // Object Relational Methods ===============================================
    
    // Users -------------------------------------------------------------------
    //<editor-fold desc="Methods for User objects">

    /**
     * Creates a new empty user object associated with this database.
     * @return
     */
    public User newUser(){
        return new User().setDatabaseManager(this);
    }

    /**
     * Returns all users from the database, unsorted.
     * @return
     */
    public ArrayList<User> getAllUsers(){
        return getUsersByUsername(null, Sort.NONE, false);
    }

    /**
     * Returns all users from the database, sorted as specified.
     * @param sort
     * @return
     */
    public ArrayList<User> getAllUsers(Sort sort){
        return getUsersByUsername(null, sort, false);
    }

    /**
     * Returns all users from the database, unsorted. Option to filter out
     * inactive users.
     * @param onlyActive
     * @return
     */
    public ArrayList<User> getAllUsers(boolean onlyActive){
        return getUsersByUsername(null, Sort.NONE, onlyActive);
    }

    /**
     * Returns all users from the database, sorted as specified. Option to 
     * filter out inactive users.
     * @param sort
     * @param onlyActive
     * @return
     */
    public ArrayList<User> getAllUsers(Sort sort, boolean onlyActive){
        return getUsersByUsername(null, sort, onlyActive);
    }
    
    /**
     * Returns all active users with a username LIKE the specified username, 
     * unsorted.
     * @param username
     * @return
     */
    public ArrayList<User> getUsersByUsername(String username){
        return getUsersByUsername(username, Sort.NONE, true);
    }

    /**
     * Returns all active users with a username LIKE the specified username, 
     * sorted as specified.
     * @param username
     * @param sort
     * @return
     */
    public ArrayList<User> getUsersByUsername(String username, Sort sort){
        return getUsersByUsername(username, sort, true);
    }

    /**
     * Returns all active users with a username LIKE the specified username, 
     * sorted as specified. Option to filter out inactive users.
     * @param username
     * @param sort
     * @param onlyActive
     * @return
     */
    public ArrayList<User> getUsersByUsername(String username, Sort sort, boolean onlyActive){
        String sortClause = "";
        if(sort == Sort.ASC){
            sortClause = " ORDER BY username ASC";
        }
        else if (sort == Sort.DESC){
            sortClause = " ORDER BY username DESC";
        }
        String whereActiveClause1 = "";
        String whereActiveClause2 = "";
        if(onlyActive){
            whereActiveClause1 = " WHERE active = 1";
            whereActiveClause2 = " AND active = 1";
        }
        try{
            PreparedStatement stmt;
            if(username == null){
                stmt = connection.prepareStatement("SELECT * FROM user" + whereActiveClause1 + sortClause + ";");
            }
            else{
                stmt = connection.prepareStatement("SELECT * FROM user WHERE username LIKE ?" + whereActiveClause2 + sortClause + ";");
                stmt.setString(1, "%" + username + "%");
            }
            try (ResultSet rs = stmt.executeQuery()) {
                return getUserListFromResultSet(rs);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * Returns the user with the specified username.
     * @param username
     * @return
     */
    public User getUserByUsername(String username){
        return getUserByUsername(username, true);
    }

    /**
     * Returns the user with the specified username. Option to filter out
     * inactive users.
     * @param username
     * @param onlyActive
     * @return
     */
    public User getUserByUsername(String username, boolean onlyActive){
        String whereActiveClause = "";
        if(onlyActive){
            whereActiveClause = " AND active = 1";
        }
        try{
            PreparedStatement stmt;
            if(username == null || username.trim().equals("")){
                return null;
            }
            else{
                stmt = connection.prepareStatement("SELECT * FROM user WHERE username = ?" + whereActiveClause + ";");
                stmt.setString(1, username);
            }
            try (ResultSet rs = stmt.executeQuery()) {
                if(rs.isBeforeFirst()){
                    return getUserListFromResultSet(rs).get(0);
                }
                else{
                    return null;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    private ArrayList<User> getUserListFromResultSet(ResultSet rs) throws SQLException{
        ArrayList<User> users = new ArrayList<>();
        while(rs.next()){
            User u = new User()
                .setUserId(rs.getInt("userId"))
                .setUserName(rs.getString("username"))
                .setPassword(new Password().setPasswordAndSaltHex(rs.getString("password")))
                .setActive(rs.getBoolean("active"))
                .setCreateDate((Timestamp)rs.getObject("createDate", Timestamp.class))
                .setCreatedBy(rs.getString("createBy"))
                .setLastUpdate((Timestamp)rs.getObject("lastUpdate", Timestamp.class))
                .setLastUpdateBy(rs.getString("lastUpdatedBy"))
                .setDatabaseManager(this)
                .setDirty(false);
            users.add(u);
        }
        return users;
    }

    /**
     * If the user object has modified data, this method will save it to the 
     * database. 
     * @param user
     * @return
     */
    public User saveUser(User user){
        if(user.isDirty()){
            PreparedStatement stmt;
            try{
                if(user.getUserId() < 0){
                    stmt = connection.prepareStatement("INSERT INTO `user` (userName, password, active, createBy, createDate, lastUpdate, lastUpdatedBy) VALUES (?, ?, ?, ?, ?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
                }
                else{
                    stmt = connection.prepareStatement("UPDATE `user` SET userName = ?, password = ?, active = ?, createBy = ?, createDate = ?, lastUpdate = ?, lastUpdateBy = ? WHERE userId = ?;");
                }
                stmt.setString(1, user.getUserName());
                stmt.setString(2, user.getPassword().getPasswordAndSaltHex());
                stmt.setBoolean(3, user.isActive());
                stmt.setString(4, user.getCreatedBy());
                stmt.setTimestamp(5, Timestamp.valueOf(user.getCreateDate()));
                stmt.setTimestamp(6, Timestamp.valueOf(user.getLastUpdate()));
                stmt.setString(7, user.getLastUpdateBy());
                if(user.getUserId() >= 0) stmt.setInt(8, user.getUserId());
                stmt.executeUpdate();
                if(user.getUserId() < 0){
                    try (ResultSet rs = stmt.getGeneratedKeys()) {
                        if(rs.next()){
                            user.setUserId(rs.getInt(1));
                        }
                    }
                }
                user.setDirty(false);
            } catch (SQLException ex) {
                Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return user;
    }
    //</editor-fold>
    
    // Customers ---------------------------------------------------------------
    //<editor-fold desc="Methods for Customer objects">

    /**
     * Creates an empty Customer object associated with this database.
     * @return
     */
    public Customer newCustomer(){
        return new Customer().setAddress(newAddress()).setDatabaseManager(this);
    }

    /**
     * Returns all of the customers in the database, unsorted.
     * @return
     */
    public ArrayList<Customer> getAllCustomers(){
        return getCustomersByName(null, Sort.NONE, false);
    }

    /**
     * Returns all of the customers in the database, sorted as specified.
     * @param sort
     * @return
     */
    public ArrayList<Customer> getAllCustomers(Sort sort){
        return getCustomersByName(null, sort, false);
    }

    /**
     * Returns all of the customers in the database, unsorted. Option to filter
     * out inactive customers.
     * @param onlyActive
     * @return
     */
    public ArrayList<Customer> getAllCustomers(boolean onlyActive){
        return getCustomersByName(null, Sort.NONE, onlyActive);
    }

    /**
     * Returns all of the customers in the database, sorted as specified. Option
     * to filter out inactive customers.
     * @param sort
     * @param onlyActive
     * @return
     */
    public ArrayList<Customer> getAllCustomers(Sort sort, boolean onlyActive){
        return getCustomersByName(null, sort, onlyActive);
    }
    
    /**
     * Returns all of the customers with names LIKE the specified name, unsorted.
     * @param customerName
     * @return
     */
    public ArrayList<Customer> getCustomersByCustomername(String customerName){
        return getCustomersByName(customerName, Sort.NONE, true);
    }

    /**
     * Returns all of the customers with names LIKE the specified name, sorted
     * as specified.
     * @param customerName
     * @param sort
     * @return
     */
    public ArrayList<Customer> getCustomersByCustomername(String customerName, Sort sort){
        return getCustomersByName(customerName, sort, true);
    }

    /**
     * Returns all of the customers with names LIKE the specified name, sorted
     * as specified. Option to filter out inactive customers.
     * @param name
     * @param sort
     * @param onlyActive
     * @return
     */
    public ArrayList<Customer> getCustomersByName(String name, Sort sort, boolean onlyActive){
        String sortClause = "";
        if(sort == Sort.ASC){
            sortClause = " ORDER BY customerName ASC";
        }
        else if (sort == Sort.DESC){
            sortClause = " ORDER BY customerName DESC";
        }
        String whereActiveClause1 = "";
        String whereActiveClause2 = "";
        if(onlyActive){
            whereActiveClause1 = " WHERE customer.active = 1";
            whereActiveClause2 = " AND customer.active = 1";
        }
        try{
            PreparedStatement stmt;
            if(name == null){
                stmt = connection.prepareStatement("SELECT * FROM customer INNER JOIN address ON address.addressId = customer.addressId INNER JOIN city ON city.cityId = address.cityId INNER JOIN country ON city.countryId = country.countryId" + whereActiveClause1 + sortClause + ";");
            }
            else{
                stmt = connection.prepareStatement("SELECT * FROM customer INNER JOIN address ON address.addressId = customer.addressId INNER JOIN city ON city.cityId = address.cityId INNER JOIN country ON city.countryId = country.countryId WHERE customerName LIKE ?" + whereActiveClause2 + sortClause + ";");
                stmt.setString(1, "%" + name + "%");
            }
            try (ResultSet rs = stmt.executeQuery()) {
                return getCustomerListFromResultSet(rs);
            }
            catch (Exception e){
                e.printStackTrace();
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new ArrayList<>();
    }

    /**
     * Returns the customer with the specified name.
     * @param customerName
     * @return
     */
    public Customer getCustomerByCustomername(String customerName){
        return getCustomerByCustomername(customerName, true);
    }

     /**
     * Returns the customer with the specified name. Option to filter out
     * inactive customers.
     * @param username
     * @param onlyActive
     * @return
     */
    public Customer getCustomerByCustomername(String username, boolean onlyActive){
        String whereActiveClause = "";
        if(onlyActive){
            whereActiveClause = " AND customer.active = 1";
        }
        try{
            PreparedStatement stmt;
            if(username == null || username.trim().equals("")){
                return null;
            }
            else{
                stmt = connection.prepareStatement("SELECT * FROM customer INNER JOIN address ON address.addressId = customer.addressId INNER JOIN city ON city.cityId = address.cityId INNER JOIN country ON city.countryId = country.countryId WHERE username = ?" + whereActiveClause + ";");
                stmt.setString(1, username);
            }
            try (ResultSet rs = stmt.executeQuery()) {
                return getCustomerListFromResultSet(rs).get(0);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    private ArrayList<Customer> getCustomerListFromResultSet(ResultSet rs) throws SQLException{
        ArrayList<Customer> customers = new ArrayList<>();
        while(rs.next()){
            Country co = new Country()
                .setCountryId(rs.getInt("country.countryId"))
                .setCountry(rs.getString("country"))
                .setCreateDate((Timestamp)rs.getObject("createDate", Timestamp.class))
                .setCreatedBy(rs.getString("createdBy"))
                .setLastUpdate((Timestamp)rs.getObject("lastUpdate", Timestamp.class))
                .setLastUpdateBy(rs.getString("lastUpdateBy"))
                .setDatabaseManager(this)
                .setDirty(false);
            City ci = new City()
                .setCityId(rs.getInt("city.cityId"))
                .setCity(rs.getString("city"))
                .setCountry(co)
                .setCreateDate((Timestamp)rs.getObject("createDate", Timestamp.class))
                .setCreatedBy(rs.getString("createdBy"))
                .setLastUpdate((Timestamp)rs.getObject("lastUpdate", Timestamp.class))
                .setLastUpdateBy(rs.getString("lastUpdateBy"))
                .setDatabaseManager(this)
                .setDirty(false);
            Address a = new Address()
                .setAddressId(rs.getInt("addressId"))
                .setAddress(rs.getString("address"))
                .setAddress2(rs.getString("address2"))
                .setCity(ci)
                .setPostalCode(rs.getString("postalCode"))
                .setPhone(rs.getString("phone"))
                .setCreateDate((Timestamp)rs.getObject("createDate", Timestamp.class))
                .setCreatedBy(rs.getString("createdBy"))
                .setLastUpdate((Timestamp)rs.getObject("lastUpdate", Timestamp.class))
                .setLastUpdateBy(rs.getString("lastUpdateBy"))
                .setDatabaseManager(this)
                .setDirty(false);
            Customer c = new Customer()
                .setCustomerId(rs.getInt("customerId"))
                .setCustomerName(rs.getString("customerName"))
                .setAddress(a)
                .setActive(rs.getBoolean("active"))
                .setCreateDate((Timestamp)rs.getObject("createDate", Timestamp.class))
                .setCreatedBy(rs.getString("createdBy"))
                .setLastUpdate((Timestamp)rs.getObject("lastUpdate", Timestamp.class))
                .setLastUpdateBy(rs.getString("lastUpdateBy"))
                .setDatabaseManager(this)
                .setDirty(false);

            customers.add(c);
        }
        return customers;
    }

    /**
     * If the customer object has modified data, this method will save it to the
     * database.
     * @param customer
     * @return
     */
    public Customer saveCustomer(Customer customer){
        if(customer.getAddress() != null){
            customer.getAddress().save();
        }
        if(customer.isDirty()){
            PreparedStatement stmt;
            try{
                if(customer.getCustomerId() < 0){
                    stmt = connection.prepareStatement("INSERT INTO `customer` (customerName, addressId, active, createdBy, createDate, lastUpdate, lastUpdateBy) VALUES (?, ?, ?, ?, ?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
                }
                else{
                    stmt = connection.prepareStatement("UPDATE `customer` SET customerName = ?, addressId = ?, active = ?, createdBy = ?, createDate = ?, lastUpdate = ?, lastUpdateBy = ? WHERE customerId = ?;");
                }
                stmt.setString(1, customer.getCustomerName());
                stmt.setInt(2, customer.getAddress() == null ? -1 : customer.getAddress().getAddressId());
                stmt.setBoolean(3, customer.isActive());
                stmt.setString(4, customer.getCreatedBy());
                stmt.setTimestamp(5, Timestamp.valueOf(customer.getCreateDate()));
                stmt.setTimestamp(6, Timestamp.valueOf(customer.getLastUpdate()));
                stmt.setString(7, customer.getLastUpdateBy());
                if(customer.getCustomerId() >= 0) stmt.setInt(8, customer.getCustomerId());
                stmt.executeUpdate();
                if(customer.getCustomerId() < 0){
                    try (ResultSet rs = stmt.getGeneratedKeys()) {
                        if(rs.next()){
                            customer.setCustomerId(rs.getInt(1));
                        }
                    }
                }
                customer.setDirty(false);
            } catch (SQLException ex) {
                Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
            } 
            catch (Exception e){
                e.printStackTrace();
            }
        }
        return customer;
    }

    /**
     * Deletes the specified customer from the database.
     * @param customer
     */
    public void deleteCustomer(Customer customer){
        try {
            Statement stmt = connection.createStatement();
            stmt.execute("DELETE FROM `customer` WHERE `customerId` = " + customer.getCustomerId() + ";");
            customer.getAddress().delete();
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    //</editor-fold>
    
    // Country -----------------------------------------------------------------
    //<editor-fold desc="Methods for Country objects">

    /**
     * Creates a new empty country object associated with this database.
     * @return
     */
    public Country newCountry(){
        return new Country().setDatabaseManager(this);
    }

    /**
     * Returns all countries contained in this database.
     * @return
     */
    public ArrayList<Country> getAllCountries(){
        return getCountriesByCountryName(null, Sort.NONE);
    }

    /**
     * Returns all of the countries contained in this database, sorted as specified.
     * @param sort
     * @return
     */
    public ArrayList<Country> getAllCountries(Sort sort){
        return getCountriesByCountryName(null, sort);
    }
    
    /**
     * Returns all of the countries in this database with a name LIKE the name
     * specified, unsorted.
     * @param country
     * @return
     */
    public ArrayList<Country> getCountriesByCountryName(String country){
        return getCountriesByCountryName(country, Sort.NONE);
    }

    /**
     * Returns all of the countries in this database with a name LIKE the name
     * specified, sorted as specified.
     * @param country
     * @param sort
     * @return
     */
    public ArrayList<Country> getCountriesByCountryName(String country, Sort sort){
        String sortClause = "";
        if(sort == Sort.ASC){
            sortClause = " ORDER BY country ASC";
        }
        else if (sort == Sort.DESC){
            sortClause = " ORDER BY country DESC";
        }
        try{
            PreparedStatement stmt;
            if(country == null){
                stmt = connection.prepareStatement("SELECT * FROM country" + sortClause + ";");
            }
            else{
                stmt = connection.prepareStatement("SELECT * FROM country WHERE country LIKE ?" + sortClause + ";");
                stmt.setString(1, "%" + country + "%");
            }
            try (ResultSet rs = stmt.executeQuery()) {
                return getCountryListFromResultSet(rs);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * Returns the specified country from the database, or null if it is not found.
     * @param country
     * @return
     */
    public Country getCountryByCountryName(String country){
        try{
            PreparedStatement stmt;
            if(country == null || country.trim().equals("")){
                return null;
            }
            else{
                stmt = connection.prepareStatement("SELECT * FROM country WHERE country = ?;");
                stmt.setString(1, country);
            }
            try (ResultSet rs = stmt.executeQuery()) {
                return getCountryListFromResultSet(rs).get(0);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    private ArrayList<Country> getCountryListFromResultSet(ResultSet rs) throws SQLException{
        ArrayList<Country> countrys = new ArrayList<>();
        while(rs.next()){
            Country u = new Country()
                .setCountryId(rs.getInt("countryId"))
                .setCountry(rs.getString("country"))
                .setCreateDate((Timestamp)rs.getObject("createDate", Timestamp.class))
                .setCreatedBy(rs.getString("createdBy"))
                .setLastUpdate((Timestamp)rs.getObject("lastUpdate", Timestamp.class))
                .setLastUpdateBy(rs.getString("lastUpdateBy"))
                .setDatabaseManager(this)
                .setDirty(false);
            countrys.add(u);
        }
        return countrys;
    }

    /**
     * If the country has modified data, this method will save it to the database.
     * @param country
     * @return
     */
    public Country saveCountry(Country country){
        if(country.isDirty()){
            PreparedStatement stmt;
            try{
                if(country.getCountryId() < 0){
                    stmt = connection.prepareStatement("INSERT INTO `country` (country, createdBy, createDate, lastUpdate, lastUpdateBy) VALUES (?, ?, ?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
                }
                else{
                    stmt = connection.prepareStatement("UPDATE `country` SET country = ?, createdBy = ?, createDate = ?, lastUpdate = ?, lastUpdateBy = ? WHERE countryId = ?;");
                }
                stmt.setString(1, country.getCountry());
                stmt.setString(2, country.getCreatedBy());
                stmt.setTimestamp(3, Timestamp.valueOf(country.getCreateDate()));
                stmt.setTimestamp(4, Timestamp.valueOf(country.getLastUpdate()));
                stmt.setString(5, country.getLastUpdateBy());
                if(country.getCountryId() >= 0) stmt.setInt(6, country.getCountryId());
                stmt.executeUpdate();
                if(country.getCountryId() < 0){
                    try (ResultSet rs = stmt.getGeneratedKeys()) {
                        if(rs.next()){
                            country.setCountryId(rs.getInt(1));
                        }
                    }
                }
                country.setDirty(false);
            } catch (SQLException ex) {
                Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
        return country;
    }
    //</editor-fold>
    
    // Address -----------------------------------------------------------------
    //<editor-fold desc="Methods for Address objects">

    /**
     * Creates an empty address object associated with this database.
     * @return
     */
    public Address newAddress(){
        return new Address().setCity(newCity()).setDatabaseManager(this);
    }

    /**
     * Returns all addresses from the database, unsorted.
     * @return
     */
    public ArrayList<Address> getAllAddresses(){
        return getAddressesByAddress(null, Sort.NONE);
    }

    /**
     * Returns all addresses from the database, sorted as specified.
     * @param sort
     * @return
     */
    public ArrayList<Address> getAllAddresses(Sort sort){
        return getAddressesByAddress(null, sort);
    }
    
    /**
     * Returns the addresses LIKE the specified address, unsorted.
     * @param address
     * @return
     */
    public ArrayList<Address> getAddressesByAddress(String address){
        return getAddressesByAddress(address, Sort.NONE);
    }

    /**
     * Returns the addresses LIKE the specified address, sorted as specified.
     * @param address
     * @param sort
     * @return
     */
    public ArrayList<Address> getAddressesByAddress(String address, Sort sort){
        String sortClause = "";
        if(sort == Sort.ASC){
            sortClause = " ORDER BY address ASC";
        }
        else if (sort == Sort.DESC){
            sortClause = " ORDER BY address DESC";
        }
        try{
            PreparedStatement stmt;
            if(address == null){
                stmt = connection.prepareStatement("SELECT * FROM address INNER JOIN city ON city.cityId = address.cityId INNER JOIN country ON city.countryId = country.countryId" + sortClause + ";");
            }
            else{
                stmt = connection.prepareStatement("SELECT * FROM address INNER JOIN city ON city.cityId = address.cityId INNER JOIN country ON city.countryId = country.countryId WHERE address LIKE ? OR address2 LIKE ?" + sortClause + ";");
                stmt.setString(1, "%" + address + "%");
                stmt.setString(2, "%" + address + "%");
            }
            try (ResultSet rs = stmt.executeQuery()) {
                return getAddressListFromResultSet(rs);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * Returns the address specified or null if it is not found.
     * @param address
     * @return
     */
    public Address getAddressByAddressName(String address){
        try{
            PreparedStatement stmt;
            if(address == null || address.trim().equals("")){
                return null;
            }
            else{
                stmt = connection.prepareStatement("SELECT * FROM address INNER JOIN city ON city.cityId = address.cityId INNER JOIN country ON city.countryId = country.countryId WHERE address = ? OR address2 = ?;");
                stmt.setString(1, address);
                stmt.setString(2, address);
            }
            try (ResultSet rs = stmt.executeQuery()) {
                return getAddressListFromResultSet(rs).get(0);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    private ArrayList<Address> getAddressListFromResultSet(ResultSet rs) throws SQLException{
        ArrayList<Address> addresss = new ArrayList<>();
        while(rs.next()){
            Country co = new Country()
                .setCountryId(rs.getInt("country.countryId"))
                .setCountry(rs.getString("country"))
                .setCreateDate((Timestamp)rs.getObject("createDate", Timestamp.class))
                .setCreatedBy(rs.getString("createdBy"))
                .setLastUpdate((Timestamp)rs.getObject("lastUpdate", Timestamp.class))
                .setLastUpdateBy(rs.getString("lastUpdateBy"))
                .setDatabaseManager(this)
                .setDirty(false);
            City ci = new City()
                .setCityId(rs.getInt("city.cityId"))
                .setCity(rs.getString("city"))
                .setCountry(co)
                .setCreateDate((Timestamp)rs.getObject("createDate", Timestamp.class))
                .setCreatedBy(rs.getString("createdBy"))
                .setLastUpdate((Timestamp)rs.getObject("lastUpdate", Timestamp.class))
                .setLastUpdateBy(rs.getString("lastUpdateBy"))
                .setDatabaseManager(this)
                .setDirty(false);
            Address u = new Address()
                .setAddressId(rs.getInt("addressId"))
                .setAddress(rs.getString("address"))
                .setAddress2(rs.getString("address2"))
                .setCity(ci)
                .setPostalCode(rs.getString("postalCode"))
                .setPhone(rs.getString("phone"))
                .setCreateDate((Timestamp)rs.getObject("createDate", Timestamp.class))
                .setCreatedBy(rs.getString("createdBy"))
                .setLastUpdate((Timestamp)rs.getObject("lastUpdate", Timestamp.class))
                .setLastUpdateBy(rs.getString("lastUpdateBy"))
                .setDatabaseManager(this)
                .setDirty(false);
            addresss.add(u);
        }
        return addresss;
    }

    /**
     * If the address has modified data, this method will save it to the database.
     * @param address
     * @return
     */
    public Address saveAddress(Address address){
        if(address.getCity() != null){
            address.getCity().save();
        }
        if(address.isDirty()){
            PreparedStatement stmt;
            try{
                if(address.getAddressId() < 0){
                    stmt = connection.prepareStatement("INSERT INTO `address` (address, address2, cityId, postalCode, phone, createdBy, createDate, lastUpdate, lastUpdateBy) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
                }
                else{
                    stmt = connection.prepareStatement("UPDATE `address` SET address = ?, address2 = ?, cityId = ?, postalCode = ?, phone = ?, createdBy = ?, createDate = ?, lastUpdate = ?, lastUpdateBy = ? WHERE addressId = ?;");
                }
                stmt.setString(1, address.getAddress());
                stmt.setString(2, address.getAddress2());
                stmt.setInt(3, address.getCity() == null ? -1 : address.getCity().getCityId());
                stmt.setString(4, address.getPostalCode());
                stmt.setString(5, address.getPhone());
                stmt.setString(6, address.getCreatedBy());
                stmt.setTimestamp(7, Timestamp.valueOf(address.getCreateDate()));
                stmt.setTimestamp(8, Timestamp.valueOf(address.getLastUpdate()));
                stmt.setString(9, address.getLastUpdateBy());
                if(address.getAddressId() >= 0) stmt.setInt(10, address.getAddressId());
                stmt.executeUpdate();
                if(address.getAddressId() < 0){
                    try (ResultSet rs = stmt.getGeneratedKeys()) {
                        if(rs.next()){
                            address.setAddressId(rs.getInt(1));
                        }
                    }
                }
                address.setDirty(false);
            } catch (SQLException ex) {
                Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
        return address;
    }

    /**
     * Deletes the address from the database.
     * @param address
     */
    public void deleteAddess(Address address){
        try {
            Statement stmt = connection.createStatement();
            stmt.execute("DELETE FROM `address` WHERE `addressId` = " + address.getAddressId() + ";");
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
       
    //</editor-fold>

    // City -----------------------------------------------------------------
    //<editor-fold desc="Methods for City objects">

    /**
     * Creates and empty city object associated with this database.
     * @return
     */
    public City newCity(){
        return new City().setCountry(newCountry()).setDatabaseManager(this);
    }

    /**
     * Returns all of the cities contained in the database, unsorted.
     * @return
     */
    public ArrayList<City> getAllCities(){
        return getCitiesByCity(null, Sort.NONE);
    }

    /**
     * Returns all of the cities contained in the database, sorted as specified.
     * @param sort
     * @return
     */
    public ArrayList<City> getAllCities(Sort sort){
        return getCitiesByCity(null, sort);
    }

    /**
     * Returns the cities named LIKE the specified city from the database, unsorted.
     * @param city
     * @return
     */
    public ArrayList<City> getCitiesByCity(String city){
        return getCitiesByCity(city, Sort.NONE);
    }

    /**
     * Returns the cities named LIKE the specified city from the database, sorted
     * as specified.
     * @param city
     * @param sort
     * @return
     */
    public ArrayList<City> getCitiesByCity(String city, Sort sort){
        String sortClause = "";
        if(sort == Sort.ASC){
            sortClause = " ORDER BY city ASC";
        }
        else if (sort == Sort.DESC){
            sortClause = " ORDER BY city DESC";
        }
        try{
            PreparedStatement stmt;
            if(city == null){
                stmt = connection.prepareStatement("SELECT * FROM city INNER JOIN country ON city.countryId = country.countryId" + sortClause + ";");
            }
            else{
                stmt = connection.prepareStatement("SELECT * FROM city INNER JOIN country ON city.countryId = country.countryId WHERE city LIKE ?" + sortClause + ";");
                stmt.setString(1, "%" + city + "%");
            }
            try (ResultSet rs = stmt.executeQuery()) {
                return getCityListFromResultSet(rs);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * Returns the city specified, or null if it is not found.
     * @param city
     * @return
     */
    public City getCityByCityName(String city){
        try{
            PreparedStatement stmt;
            if(city == null || city.trim().equals("")){
                return null;
            }
            else{
                stmt = connection.prepareStatement("SELECT * FROM city INNER JOIN country ON city.countryId = country.countryId WHERE city = ?;");
                stmt.setString(1, city);
            }
            try (ResultSet rs = stmt.executeQuery()) {
                return getCityListFromResultSet(rs).get(0);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    private ArrayList<City> getCityListFromResultSet(ResultSet rs) throws SQLException{
        ArrayList<City> cities = new ArrayList<>();
        while(rs.next()){
            Country co = new Country()
                .setCountryId(rs.getInt("country.countryId"))
                .setCountry(rs.getString("country"))
                .setCreateDate((Timestamp)rs.getObject("createDate", Timestamp.class))
                .setCreatedBy(rs.getString("createdBy"))
                .setLastUpdate((Timestamp)rs.getObject("lastUpdate", Timestamp.class))
                .setLastUpdateBy(rs.getString("lastUpdateBy"))
                .setDatabaseManager(this)
                .setDirty(false);
            City ci = new City()
                .setCityId(rs.getInt("city.cityId"))
                .setCity(rs.getString("city"))
                .setCountry(co)
                .setCreateDate((Timestamp)rs.getObject("createDate", Timestamp.class))
                .setCreatedBy(rs.getString("createdBy"))
                .setLastUpdate((Timestamp)rs.getObject("lastUpdate", Timestamp.class))
                .setLastUpdateBy(rs.getString("lastUpdateBy"))
                .setDatabaseManager(this)
                .setDirty(false);
            cities.add(ci);
        }
        return cities;
    }

    /**
     * If the city has modified data, this method will save it to the database.
     * @param city
     * @return
     */
    public City saveCity(City city){
        if(city.getCountry() != null){
            city.getCountry().save();
        }
        if(city.isDirty()){
            PreparedStatement stmt;
            try{
                if(city.getCityId() < 0){
                    stmt = connection.prepareStatement("INSERT INTO `city` (city, countryId, createdBy, createDate, lastUpdate, lastUpdateBy) VALUES (?, ?, ?, ?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
                }
                else{
                    stmt = connection.prepareStatement("UPDATE `city` SET city = ?, countryId = ?, createdBy = ?, createDate = ?, lastUpdate = ?, lastUpdateBy = ? WHERE cityId = ?;");
                }
                stmt.setString(1, city.getCity());
                stmt.setInt(2, city.getCountry() == null ? -1 : city.getCountry().getCountryId());
                stmt.setString(3, city.getCreatedBy());
                stmt.setTimestamp(4, Timestamp.valueOf(city.getCreateDate()));
                stmt.setTimestamp(5, Timestamp.valueOf(city.getLastUpdate()));
                stmt.setString(6, city.getLastUpdateBy());
                if(city.getCityId() >= 0) stmt.setInt(7, city.getCityId());
                stmt.executeUpdate();
                if(city.getCityId() < 0){
                    try (ResultSet rs = stmt.getGeneratedKeys()) {
                        if(rs.next()){
                            city.setCityId(rs.getInt(1));
                        }
                    }
                }
                city.setDirty(false);
            } catch (SQLException ex) {
                Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
        return city;
    }
    //</editor-fold>
    
    // Appointment -----------------------------------------------------------------
    //<editor-fold desc="Methods for Appointment objects">

    /**
     * Creates an empty appointment object associated with this database and the
     * logged in user.
     * @return
     */
    public Appointment newAppointment(){
        return new Appointment()
                .setDatabaseManager(this)
                .setLang(lang)
                .setUser(loggedInUser)
                .setDirty(false);
    }
    
    /**
     * Returns all of the appointments that start on the specified date, sorted
     * ascending and scoped to the logged in user.
     * @param date
     * @return
     */
    public ArrayList<Appointment> getAppointmentsOnDate(LocalDate date) {
        return getAppointmentsOnDate(date, Sort.ASC);
    }
    
    /**
     * Returns all of the appointments that start on the specified date, sorted
     * as specified and scoped to the logged in user.
     * @param date
     * @param sort
     * @return
     */
    public ArrayList<Appointment> getAppointmentsOnDate(LocalDate date, Sort sort) {
        String sortClause = "";
        if(sort == Sort.ASC){
            sortClause = " ORDER BY TIME(`start`) ASC";
        }
        else if (sort == Sort.DESC){
            sortClause = " ORDER BY TIME(`start`) DESC";
        }
        try{
            PreparedStatement stmt;
            if(date == null){
                return null;
            }
            else{
                stmt = connection.prepareStatement("SELECT * FROM appointment INNER JOIN customer ON customer.customerId = appointment.customerId INNER JOIN address ON address.addressId = customer.addressId INNER JOIN city ON city.cityId = address.cityId INNER JOIN country ON city.countryId = country.countryId WHERE DATE(`start`) = ? AND userId = ? OR userId = NULL" + sortClause + ";");
                stmt.setString(1, date.format(DateTimeFormatter.ISO_DATE));
                stmt.setInt(2, this.loggedInUser.getUserId());
            }
            try (ResultSet rs = stmt.executeQuery()) {
                return getAppointmentListFromResultSet(rs);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    /**
     * Returns all of the appointments between the two DateTimes, sorted
     * ascending and scoped to the logged in user.
     * @param start
     * @param end
     * @return
     */
    public ArrayList<Appointment> getAppointmentsBetween(LocalDateTime start, LocalDateTime end) {
        return getAppointmentsBetween(start, end, Sort.ASC);
    }
    
    /**
     * Returns all of the appointments between the two DateTimes, sorted
     * as specified and scoped to the logged in user.
     * @param start
     * @param end
     * @param sort
     * @return
     */
    public ArrayList<Appointment> getAppointmentsBetween(LocalDateTime start, LocalDateTime end, Sort sort) {
        String sortClause = "";
        if(sort == Sort.ASC){
            sortClause = " ORDER BY TIME(`start`) ASC";
        }
        else if (sort == Sort.DESC){
            sortClause = " ORDER BY TIME(`start`) DESC";
        }
        try{
            PreparedStatement stmt;
            if(start == null || end == null){
                return null;
            }
            else{
                stmt = connection.prepareStatement("SELECT * FROM appointment INNER JOIN customer ON customer.customerId = appointment.customerId INNER JOIN address ON address.addressId = customer.addressId INNER JOIN city ON city.cityId = address.cityId INNER JOIN country ON city.countryId = country.countryId WHERE `start` >= ? AND `start` <= ? AND userId = ? OR userId = NULL" + sortClause + ";");
                stmt.setTimestamp(1, Timestamp.valueOf(start));
                stmt.setTimestamp(2, Timestamp.valueOf(end));
                stmt.setInt(3, this.loggedInUser.getUserId());
            }
            try (ResultSet rs = stmt.executeQuery()) {
                return getAppointmentListFromResultSet(rs);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    private ArrayList<Appointment> getAppointmentListFromResultSet(ResultSet rs) throws SQLException{
        ArrayList<Appointment> appointments = new ArrayList<>();
        while(rs.next()){
            Country co = new Country()
                .setCountryId(rs.getInt("country.countryId"))
                .setCountry(rs.getString("country"))
                .setCreateDate((Timestamp)rs.getObject("createDate", Timestamp.class))
                .setCreatedBy(rs.getString("createdBy"))
                .setLastUpdate((Timestamp)rs.getObject("lastUpdate", Timestamp.class))
                .setLastUpdateBy(rs.getString("lastUpdateBy"))
                .setDatabaseManager(this)
                .setDirty(false);
            City ci = new City()
                .setCityId(rs.getInt("city.cityId"))
                .setCity(rs.getString("city"))
                .setCountry(co)
                .setCreateDate((Timestamp)rs.getObject("createDate", Timestamp.class))
                .setCreatedBy(rs.getString("createdBy"))
                .setLastUpdate((Timestamp)rs.getObject("lastUpdate", Timestamp.class))
                .setLastUpdateBy(rs.getString("lastUpdateBy"))
                .setDatabaseManager(this)
                .setDirty(false);
            Address a = new Address()
                .setAddressId(rs.getInt("addressId"))
                .setAddress(rs.getString("address"))
                .setAddress2(rs.getString("address2"))
                .setCity(ci)
                .setPostalCode(rs.getString("postalCode"))
                .setPhone(rs.getString("phone"))
                .setCreateDate((Timestamp)rs.getObject("createDate", Timestamp.class))
                .setCreatedBy(rs.getString("createdBy"))
                .setLastUpdate((Timestamp)rs.getObject("lastUpdate", Timestamp.class))
                .setLastUpdateBy(rs.getString("lastUpdateBy"))
                .setDatabaseManager(this)
                .setDirty(false);
            Customer c = new Customer()
                .setCustomerId(rs.getInt("customerId"))
                .setCustomerName(rs.getString("customerName"))
                .setAddress(a)
                .setActive(rs.getBoolean("active"))
                .setCreateDate((Timestamp)rs.getObject("createDate", Timestamp.class))
                .setCreatedBy(rs.getString("createdBy"))
                .setLastUpdate((Timestamp)rs.getObject("lastUpdate", Timestamp.class))
                .setLastUpdateBy(rs.getString("lastUpdateBy"))
                .setDatabaseManager(this)
                .setDirty(false);
            Appointment ap = new Appointment()
                .setAppointmentId(rs.getInt("appointmentId"))
                .setCustomer(c)
                .setUser(rs.getInt("userId") == 0 ? null : this.loggedInUser)
                .setTitle(rs.getString("title"))
                .setDescription(rs.getString("description"))
                .setLocation(rs.getString("location"))
                .setContact(rs.getString("contact"))
                .setType(rs.getString("type"))
                .setUrl(rs.getString("url"))
                .setStart((Timestamp)rs.getObject("start", Timestamp.class))
                .setEnd((Timestamp)rs.getObject("end", Timestamp.class))
                .setCreateDate((Timestamp)rs.getObject("createDate", Timestamp.class))
                .setCreatedBy(rs.getString("createdBy"))
                .setLastUpdate((Timestamp)rs.getObject("lastUpdate", Timestamp.class))
                .setLastUpdateBy(rs.getString("lastUpdateBy"))
                .setLang(lang)
                .setDatabaseManager(this)
                .setDirty(false);
            
            appointments.add(ap);
        }
        return appointments;
    }

    /**
     * Returns a list of the distinct appointment types in the appointments table,
     * sorted as specified.
     * @param sort
     * @return
     */
    public ArrayList<String> getAppointmentTypes(Sort sort){
        ArrayList<String> typeList = new ArrayList<>();
        String sortClause = "";
        if(sort == Sort.ASC){
            sortClause = " ORDER BY `type` ASC";
        }
        else if (sort == Sort.DESC){
            sortClause = " ORDER BY `type` DESC";
        }
        try{
            PreparedStatement stmt;
            stmt = connection.prepareStatement("SELECT DISTINCT `type` FROM appointment" + sortClause + ";");
            try (ResultSet rs = stmt.executeQuery()) {
                while(rs.next()){
                    typeList.add(rs.getString("type"));
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return typeList;
    }
    
    /**
     * Returns the number of appointments of the specified type between the two
     * dates for the specified user.
     * @param type
     * @param user
     * @param start
     * @param end
     * @return
     */
    public int getAppointmentTypeCount(String type, User user, LocalDateTime start, LocalDateTime end) {
        int count = 0;
        try{
            PreparedStatement stmt;
            stmt = connection.prepareStatement("SELECT COUNT(`type`) AS type_count FROM appointment WHERE `type` = ? AND userId = ? AND start >= ? AND end <= ?;");
            stmt.setString(1, type);
            stmt.setInt(2, user.getUserId());
            stmt.setTimestamp(3, Timestamp.valueOf(start));
            stmt.setTimestamp(4, Timestamp.valueOf(end));
            try (ResultSet rs = stmt.executeQuery()) {
                if(rs.first()){
                    count = rs.getInt("type_count");
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return count;
    }
    
    /**
     * Returns the total number of appointments for the specified user between
     * the two dates.
     * @param user
     * @param start
     * @param end
     * @return
     */
    public int getAppointmentTotal(User user, LocalDateTime start, LocalDateTime end) {
        int count = 0;
        try{
            PreparedStatement stmt;
            stmt = connection.prepareStatement("SELECT COUNT(`appointmentId`) AS appt_count FROM appointment WHERE userId = ? AND start >= ? AND end <= ?;");
            stmt.setInt(1, user.getUserId());
            stmt.setTimestamp(2, Timestamp.valueOf(start));
            stmt.setTimestamp(3, Timestamp.valueOf(end));
            try (ResultSet rs = stmt.executeQuery()) {
                if(rs.first()){
                    count = rs.getInt("appt_count");
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return count;
    }
    
    /**
     * If the appointment has modified data, this method will save it to the
     * database.
     * @param appointment
     * @return
     */
    public Appointment saveAppointment(Appointment appointment){
        if(appointment.isDirty()){
            PreparedStatement stmt;
            try{
                if(appointment.getAppointmentId() < 0){
                    stmt = connection.prepareStatement("INSERT INTO `appointment` (customerId, userId, title, description, location, contact, type, url, start, end, createdBy, createDate, lastUpdate, lastUpdateBy) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
                }
                else{
                    stmt = connection.prepareStatement("UPDATE `appointment` SET customerId = ?, userId = ?, title = ?, description = ?, location = ?, contact = ?, type = ?, url = ?, start = ?, end = ?, createdBy = ?, createDate = ?, lastUpdate = ?, lastUpdateBy = ? WHERE appointmentId = ?;");
                }
                stmt.setInt(1, appointment.getCustomer() == null ? -1 : appointment.getCustomer().getCustomerId());
                stmt.setInt(2, appointment.getUser() == null ? -1 : appointment.getUser().getUserId());
                stmt.setString(3, appointment.getTitle());
                stmt.setString(4, appointment.getDescription());
                stmt.setString(5, appointment.getLocation());
                stmt.setString(6, appointment.getContact());
                stmt.setString(7, appointment.getType());
                stmt.setString(8, appointment.getUrl());
                stmt.setTimestamp(9, Timestamp.valueOf(appointment.getStart()));
                stmt.setTimestamp(10, Timestamp.valueOf(appointment.getEnd()));
                stmt.setString(11, appointment.getCreatedBy());
                stmt.setTimestamp(12, Timestamp.valueOf(appointment.getCreateDate()));
                stmt.setTimestamp(13, Timestamp.valueOf(appointment.getLastUpdate()));
                stmt.setString(14, appointment.getLastUpdateBy());
                if(appointment.getAppointmentId() >= 0) stmt.setInt(15, appointment.getAppointmentId());
                stmt.executeUpdate();
                if(appointment.getAppointmentId() < 0){
                    try (ResultSet rs = stmt.getGeneratedKeys()) {
                        if(rs.next()){
                            appointment.setAppointmentId(rs.getInt(1));
                        }
                    }
                }
                appointment.setDirty(false);
            } catch (SQLException ex) {
                Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
        return appointment;
    }
    
    /**
     * Deletes the appointment from the database.
     * @param appointment
     */
    public void deleteAppointment(Appointment appointment){
        try {
            Statement stmt = connection.createStatement();
            stmt.execute("DELETE FROM `appointment` WHERE `appointmentId` = " + appointment.getAppointmentId() + ";");
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    //</editor-fold>
    
    /**
     * Returns the business opening hours for the specified day of the week.
     * In a real application, the business hours would be stored in the database
     * and the general manager would have the ability to modify them. Hard coding
     * them into the application is bad practice, but since the database provided
     * doesn't supply a place to store the business hours, I will put them here.
     * I chose to put them in the DatabaseManager class to allow future implementation
     * without a ton of refactoring.
     * @param day
     * @return 
     */
    public LocalTime getOpenTime(DayOfWeek day){
        switch(day){
            case MONDAY:
            case TUESDAY:
            case WEDNESDAY:
            case THURSDAY:
            case FRIDAY:
                return LocalTime.of(8, 0);
            case SATURDAY:
                return LocalTime.of(9, 0);
            default:
                return null;
        }
    }
    
    /**
     * Returns the business closing hours for the specified day of the week.
     * In a real application, the business hours would be stored in the database
     * and the general manager would have the ability to modify them. Hard coding
     * them into the application is bad practice, but since the database provided
     * doesn't supply a place to store the business hours, I will put them here.
     * I chose to put them in the DatabaseManager class to allow future implementation
     * without a ton of refactoring.
     * @param day
     * @return 
     */
    public LocalTime getCloseTime(DayOfWeek day){
        switch(day){
            case MONDAY:
            case TUESDAY:
            case WEDNESDAY:
            case THURSDAY:
            case FRIDAY:
                return LocalTime.of(18, 0);
            case SATURDAY:
                return LocalTime.of(17, 0);
            default:
                return null;
        }
    }
    
    // Getters and Setters =====================================================

    /**
     * Returns the connection status of this DatabaseManager object.
     * @return
     */
    public ConnectionStatus getConnectionStatus() {
        return connectionStatus;
    }

    private void setConnectionStatus(ConnectionStatus connectionStatus) {
        this.connectionStatus = connectionStatus;
    }
    
    /**
     * Returns the database name.
     * @return
     */
    public String getDatabaseName() {
        return databaseName;
    }

    /**
     * Sets the database name.
     * @param databaseName
     */
    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    /**
     * Returns the database username.
     * @return
     */
    public String getDatabaseUsername() {
        return databaseUsername;
    }

    /**
     * Sets the database username.
     * @param databaseUsername
     */
    public void setDatabaseUsername(String databaseUsername) {
        this.databaseUsername = databaseUsername;
    }

    /**
     * Returns the database password.
     * @return
     */
    public String getDatabasePassword() {
        return databasePassword;
    }

    /**
     * Sets the database password.
     * @param databasePassword
     */
    public void setDatabasePassword(String databasePassword) {
        this.databasePassword = databasePassword;
    }
    
    /**
     * Returns the database location.
     * @return
     */
    public String getDatabaseLocation() {
        return databaseLocation;
    }

    /**
     * Sets the database location.
     * @param databaseLocation
     */
    public void setDatabaseLocation(String databaseLocation) {
        this.databaseLocation = databaseLocation;
    }

    /**
     * Sets the language resource bundle.
     * @param lang
     */
    public void setLang(ResourceBundle lang) {
        this.lang = lang;
    }

    /**
     * Returns the logged in user.
     * @return
     */
    public User getLoggedInUser() {
        return loggedInUser;
    }

    /**
     * Sets the logged in user.
     * @param loggedInUser
     */
    public void setLoggedInUser(User loggedInUser) {
        this.loggedInUser = loggedInUser;
    }   
}
