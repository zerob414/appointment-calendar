package appointmentcalendar;

import dbobjects.Appointment;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import javafx.scene.Node;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.OverrunStyle;

/**
 * Creates a hyperlink associated with an appointment object. This node will
 * automatically set it's display text from the appointment associated with it
 * in either a small, medium or large format.
 * @author Eric Boring <eboring@wgu.edu>
 */
public class AppointmentHyperlink extends Hyperlink{
    private Appointment appointment;

    /**
     * Used to determine the amount of information to display about the associated
     * appointment.
     */
    public enum Size {

        /**
         * Displays only the appointment title.
         */
        SMALL, 

        /**
         * Displays the appointment title and it's start time.
         */
        MEDIUM, 

        /**
         * Displays the appointment title, it's start and end times, and the
         * appointment's description.
         */
        LARGE};
    private Size size = Size.SMALL;
    
    /**
     * Constructs an AppointmentHyperlink object with a default size of SMALL.
     * @param appointment
     */
    public AppointmentHyperlink(Appointment appointment){
        this(appointment, Size.SMALL);
    }
    
    /**
     * Constructs an AppointmentHyperlink object with the specified size.
     * @param appointment
     * @param size
     */
    public AppointmentHyperlink(Appointment appointment, Size size){
        super(appointment.getTitle());
        this.appointment = appointment;
        setSize(size);
        this.setupStyleReset();
        this.setTextOverrun(OverrunStyle.ELLIPSIS);
    }

    /**
     * Constructs and empty AppointmentHyperlink.
     */
    public AppointmentHyperlink() {
        super();
        this.setupStyleReset();
        this.setTextOverrun(OverrunStyle.ELLIPSIS);
    }

    /**
     * Constructs and AppointmentHyperlink with the specified text. The text
     * will be overwritten if setAppointment() is called to associate an
     * appointment with this node.
     * @param text
     */
    public AppointmentHyperlink(String text) {
        super(text);
        this.setupStyleReset();
    }

    /**
     * Constructs and AppointmentHyperlink with the specified text and a graphic.
     * The text will be overwritten if setAppointment() is called to associate 
     * an appointment with this node. 
     * @param text
     * @param graphic
     */
    public AppointmentHyperlink(String text, Node graphic) {
        super(text, graphic);
        this.setupStyleReset();
    }
    
    private void setupStyleReset(){
        //Quickly adding an event that will reset the hyperlink's visual appearnce when it loses focus.
        this.focusedProperty().addListener((obsValue, oldValue, newValue) -> {
            if(!newValue){
                this.setStyle("");
            }
        });
    }
    
    /**
     * Returns the appointment associated with this AppointmentHyperlink object.
     * @return
     */
    public Appointment getAppointment() {
        return appointment;
    }

    /**
     * Sets the appointment associated with this AppointmentHyperlink object.
     * @param appointment
     */
    public void setAppointment(Appointment appointment) {
        this.appointment = appointment;
        updateText();
    }

    /**
     * Returns the size of the information displayed on this AppointmentHyperlink
     * object.
     * @return
     */
    public Size getSize() {
        return size;
    }

    /**
     * Sets the size of the information displayed on this AppointmentHyperlink
     * object.
     * @param size
     */
    public void setSize(Size size) {
        this.size = size;
        updateText();
    }
    
    private void updateText(){
        if(appointment != null){
            switch (size) {
                case LARGE:
                    this.setText(appointment.getStart().toLocalTime().format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT)) + " - " + appointment.getEnd().toLocalTime().format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT)) + " \u21d2 " + appointment.getTitle() + ": " + appointment.getDescription());
                    break;
                case MEDIUM:
                    this.setText(appointment.getStart().toLocalTime().format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT)) + " \u21d2 " + appointment.getTitle());
                    break;
                default:                    
                    this.setText(appointment.getTitle());
                    break;
            }
        }
    }
    
}
