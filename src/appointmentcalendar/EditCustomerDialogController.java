package appointmentcalendar;

import dbobjects.City;
import dbobjects.Country;
import dbobjects.Customer;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 * The controller for the edit customer dialog.
 * @author Eric Boring <eboring@wgu.edu>
 */
public class EditCustomerDialogController implements Initializable {
    private Stage stage;
    private Customer customer;
    private MainWindowController mainWindowController;
    
    @FXML
    private ComboBox<City> cmbCity;

    @FXML
    private TextField txtPhone;

    @FXML
    private Button btnCancel;

    @FXML
    private Button btnSave;

    @FXML
    private TextField txtName;

    @FXML
    private ComboBox<Country> cmbCountry;

    @FXML
    private TextField txtAddress;

    @FXML
    private TextField txtAddress2;

    @FXML
    private Label lblTitle;

    @FXML
    private TextField txtPostalCode;
    
    @FXML
    private ImageView imgInvalidName;
        
    @FXML
    private ImageView imgInvalidCity;
     
    @FXML
    private ImageView imgInvalidCountry;
      
    @FXML
    private ImageView imgInvalidAddress;
      
    @FXML
    private ImageView imgInvalidPhone;
      
    @FXML
    private ImageView imgInvalidPostalCode;
    
    /**
     * Ensures the user has enter data for the customer and attempts to match
     * any typed in City and Country with and existing City and Country.
     * @param e
     */
    @FXML
    public void validateInput(Event e){
        boolean valid = true;
        if(txtName.getText().isEmpty())valid = false;
        imgInvalidName.setVisible(txtName.getText().isEmpty());
        if(txtAddress.getText().isEmpty())valid = false;
        imgInvalidAddress.setVisible(txtAddress.getText().isEmpty());
        Object city = cmbCity.getValue();
        imgInvalidCity.setVisible(city == null || city.equals(""));
        if(city == null || city.equals(""))valid = false;
        if(city instanceof String){
            ObservableList<City> cities = cmbCity.getItems();
            for(City c : cities){
                if(c.getCity().equalsIgnoreCase((String)city)){
                    cmbCity.setValue(c);
                    break;
                }
            }
            Object newCity = cmbCity.getValue();
            if(newCity instanceof String){
                cmbCity.setValue(this.mainWindowController.getAppointmentCalendarApp().getDatabaseManager().newCity().setCity(newCity.toString()));
            }
        }
        Object country = cmbCountry.getValue();
        imgInvalidCountry.setVisible(country == null || country.equals(""));
        if(country == null || country.equals(""))valid = false;
        if(country instanceof String){
            ObservableList<Country> countries = cmbCountry.getItems();
            for(Country c : countries){
                if(c.getCountry().equalsIgnoreCase((String)country)){
                    cmbCountry.setValue(c);
                    break;
                }
            }
            Object newCountry = cmbCountry.getValue();
            if(newCountry instanceof String){
                cmbCountry.setValue(this.mainWindowController.getAppointmentCalendarApp().getDatabaseManager().newCountry().setCountry(newCountry.toString()));
            }
        }
        imgInvalidPostalCode.setVisible(txtPostalCode.getText().isEmpty());
        if(txtPostalCode.getText().isEmpty())valid = false;
        imgInvalidPhone.setVisible(txtPhone.getText().isEmpty());
        if(txtPhone.getText().isEmpty())valid = false;
        btnSave.setDisable(!valid);
    }
    
    @FXML
    void saveClicked(ActionEvent event) {
        validateInput(null);
        customer.setCustomerName(txtName.getText())
                .getAddress().setAddress(txtAddress.getText())
                             .setAddress2(txtAddress2.getText())
                             .setPostalCode(txtPostalCode.getText())
                             .setPhone(txtPhone.getText())
                             .setCity(cmbCity.getValue())
                                       .getCity().setCountry(cmbCountry.getValue());
        customer.save();
        this.stage.close();
        mainWindowController.refreshList();
        mainWindowController.updateCustomerDisplay();
    }

    @FXML
    void cancelClicked(ActionEvent event) {
        this.stage.close();
    }
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {   
    }  

    /**
     * Returns the stage associated with this controller.
     * @return
     */
    public Stage getStage() {
        return stage;
    }

    /**
     * Sets the stage associated with this controller.
     * @param stage
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    /**
     * Returns the customer being edited on this dialog.
     * @return
     */
    public Customer getCustomer() {
        return customer;
    }

    /**
     * Sets the customer being edited on this dialog.
     * @param customer
     */
    public void setCustomer(Customer customer) {
        this.customer = customer;
        txtName.setText(customer.getCustomerName());
        txtAddress.setText(customer.getAddress().getAddress());
        txtAddress2.setText(customer.getAddress().getAddress2());
        cmbCity.setValue(customer.getAddress().getCity());
        cmbCountry.setValue(customer.getAddress().getCity().getCountry());
        txtPostalCode.setText(customer.getAddress().getPostalCode());
        txtPhone.setText(customer.getAddress().getPhone());
    }

    /**
     * Returns the MainWindowController that is the parent of this controller.
     * @return
     */
    public MainWindowController getMainWindowController() {
        return mainWindowController;
    }

    /**
     * Sets the MainWindowController that is the parent of this controller.
     * @param mainWindowController
     */
    public void setMainWindowController(MainWindowController mainWindowController) {
        this.mainWindowController = mainWindowController;
    }

    /**
     * Sets the list of cities to use for the City combobox.
     * @param cities
     */
    public void setCitiesList(ObservableList<City> cities) {
        cmbCity.setItems(cities);
    }

    /**
     * Sets the list of countries to use for the Country combobox.
     * @param countries
     */
    public void setCountriesList(ObservableList<Country> countries) {
        cmbCountry.setItems(countries);
    }
   
    
}
