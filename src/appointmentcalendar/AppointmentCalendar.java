package appointmentcalendar;

import dbobjects.Password;
import dbobjects.User;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Initializes the application resources and facilitates switching between the 
 * main windows.
 * @author Eric Boring <eboring@wgu.edu>
 */
public class AppointmentCalendar extends Application {
    private DatabaseManager databaseManager = new DatabaseManager();
    
    private Stage loginStage;
    private Stage mainStage;
    
    private ResourceBundle langEnglish = ResourceBundle.getBundle("bundles.LangBundle", new Locale("en"));
    private ResourceBundle langGerman = ResourceBundle.getBundle("bundles.LangBundle", new Locale("de"));
    private ResourceBundle langSelected = langEnglish;
    
    @Override
    public void start(Stage stage) throws Exception {
        switchToLoginDialog();
        
        // Uncomment the following blocks to insert test users into the database if they are not there already.
        
        databaseManager.connect();
        databaseManager.setLang(langSelected);
        
//        User test = databaseManager.newUser().setUserName("test").setPassword( new Password().setAlgorithm(Password.SHA_1).setPasswordPlainText("test", "test".getBytes())).setActive(true).setCreatedBy("ricksanchez").setCreateDate(LocalDateTime.now()).setLastUpdate(LocalDateTime.now()).setLastUpdateBy("ricksanchez").save();
//        User rickSanchez = databaseManager.newUser().setUserName("ricksanchez").setPassword( new Password().setAlgorithm(Password.SHA_1).setPasswordPlainText("portalgun", "beeeelch".getBytes())).setActive(true).setCreatedBy("ricksanchez").setCreateDate(LocalDateTime.now()).setLastUpdate(LocalDateTime.now()).setLastUpdateBy("ricksanchez").save();
//        User mortySmith = databaseManager.newUser().setUserName("mortysmith").setPassword( new Password().setAlgorithm(Password.SHA_1).setPasswordPlainText("megaseeds", "jessicasfeet".getBytes())).setActive(true).setCreatedBy("ricksanchez").setCreateDate(LocalDateTime.now()).setLastUpdate(LocalDateTime.now()).setLastUpdateBy("ricksanchez").save();
//        User jerrySmith = databaseManager.newUser().setUserName("jerrysmith").setPassword( new Password().setAlgorithm(Password.SHA_1).setPasswordPlainText("apples", "sigerians".getBytes())).setActive(true).setCreatedBy("mortysmith").setCreateDate(LocalDateTime.now()).setLastUpdate(LocalDateTime.now()).setLastUpdateBy("mortysmith").save();
//        User mrMeeseeks = databaseManager.newUser().setUserName("mrmeeseeks").setPassword( new Password().setAlgorithm(Password.SHA_1).setPasswordPlainText("lookatme", "twostrokes".getBytes())).setActive(true).setCreatedBy("jerrysmith").setCreateDate(LocalDateTime.now()).setLastUpdate(LocalDateTime.now()).setLastUpdateBy("jerrysmith").save();
//        User xenonBloom = databaseManager.newUser().setUserName("xenonbloom").setPassword( new Password().setAlgorithm(Password.SHA_1).setPasswordPlainText("pancreaspirates", "anatomypark".getBytes())).setActive(true).setCreatedBy("ricksanchez").setCreateDate(LocalDateTime.now()).setLastUpdate(LocalDateTime.now()).setLastUpdateBy("ricksanchez").save();
        
        // End of dummy data statements
    }

    /**
     * Closes the login dialog and shows the main application window.
     * @param user
     */
    public void switchToMainWindow(User user){
        mainStage = new Stage();
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("MainWindow.fxml"), langSelected);
            loader.load();
            Scene scene = new Scene(loader.getRoot());
            mainStage.setTitle(langSelected.getString("apptitle"));
            mainStage.setScene(scene);
            mainStage.show();
            
            MainWindowController mainController = loader.getController();
            mainController.setStage(mainStage);
            mainController.setLoggedInUser(user);
            mainController.setAppointmentCalendarApp(this);
            mainController.searchCustomers("");
            mainController.populateTimezoneList();
            mainController.datePicked(null);
            mainController.updateNotifications();
        } catch (IOException ex) {
            DialogPopper.showError(langSelected.getString("apptitle"), langSelected.getString("error"), langSelected.getString("errorMainWindow"), loginStage, langSelected);
            Logger.getLogger(AppointmentCalendar.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            loginStage.close();
        }
    }

    /**
     * Closes the main application window and shows the login dialog.
     */
    public void switchToLoginDialog(){
        loginStage = new Stage();
        try{
            if(Locale.getDefault().getLanguage().equals(Locale.GERMAN.getLanguage())){
                langSelected = langGerman;
            }
            FXMLLoader loader = new FXMLLoader(getClass().getResource("LoginForm.fxml"), langSelected);
            loader.load();
            Scene scene = new Scene(loader.getRoot());
            loginStage.setTitle(langSelected.getString("apptitle") + " - " + langSelected.getString("login"));
            loginStage.setScene(scene);
            loginStage.show();

            LoginFormController loginController = loader.getController();
            loginController.setStage(loginStage);
            loginController.setAppointmentCalendarApp(this);
            loginController.postShow();
        } catch (IOException ex) {
            DialogPopper.showError(langSelected.getString("apptitle"), langSelected.getString("error"), langSelected.getString("errorLoginWindow"), null, langSelected);
            Logger.getLogger(AppointmentCalendar.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally{
            if(mainStage != null){
                mainStage.close();
            }
        }
    }
    
    /**
     * Returns the database manager to be used in this running instance.
     * @return
     */
    public DatabaseManager getDatabaseManager() {
        return databaseManager;
    }

    /**
     * Sets the database manager to be used in this running instance.
     * @param databaseManager
     */
    public void setDatabaseManager(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
    }

    /**
     * Returns the English language resource bundle.
     * @return
     */
    public ResourceBundle getLangEnglish() {
        return langEnglish;
    }

    /**
     * Returns the selected language resource bundle.
     * @return
     */
    public ResourceBundle getLangSelected() {
        return langSelected;
    }

    /**
     * Returns the German language resource bundle.
     * @return
     */
    public ResourceBundle getLangGerman() {
        return langGerman;
    }    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Changes the application's language to the passed in language. If this
     * application does not support the language, English is defaulted to.
     * @param languageName
     */
    public void changeLanguage(String languageName) {
        if(languageName.equalsIgnoreCase(Locale.GERMAN.getDisplayName())){
            langSelected = langGerman;
            getDatabaseManager().setLang(langGerman);
            Locale.setDefault(Locale.GERMAN);
        }
        else{
            langSelected = langEnglish;
            getDatabaseManager().setLang(langEnglish);
            Locale.setDefault(Locale.ENGLISH);
        }
        databaseManager.setLang(langSelected);
    }
}
