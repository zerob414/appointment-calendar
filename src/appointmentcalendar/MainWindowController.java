package appointmentcalendar;

import dbobjects.Appointment;
import dbobjects.City;
import dbobjects.Country;
import dbobjects.Customer;
import dbobjects.User;
import java.awt.Dimension;
import javafx.scene.text.Font;
import java.awt.Toolkit;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TimeZone;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.print.PrinterJob;
import javafx.scene.Scene;
import javafx.scene.SubScene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;
import javafx.scene.web.WebView;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * FXML Controller class
 * The controller for the application's main window.
 * @author Eric Boring <eboring@wgu.edu>
 */
public class MainWindowController implements Initializable, ChangeListener {
    private Stage myStage;
    private AppointmentCalendar myAppointmentCalendarApp;
    private ResourceBundle selectedLang;
    private User loggedInUser;
    private DatabaseManager databaseManager;
    private Appointment selectedAppointment;
    private DayViewController dayViewController;
    private WeekViewController weekViewController;
    private ArrayList<Appointment> notificationAlreadyShown = new ArrayList<>();
    ScheduledExecutorService notificationsUpdaterExecutor = null;
    
    @FXML
    private ListView<Customer> lstContacts;

    @FXML
    private Label lblDay4;

    @FXML
    private Label lblDay5;

    @FXML
    private Label lblDay2;

    @FXML
    private Label lblDay3;

    @FXML
    private MenuItem mnuLogout;

    @FXML
    private Label lblDay1;

    @FXML
    private ToggleButton btnDayView;

    @FXML
    private Menu mnuUser;

    @FXML
    private Label lblDay6;

    @FXML
    private Label lblDay7;
    
    @FXML
    private DatePicker datePicker;

    @FXML
    private VBox week1day2;

    @FXML
    private VBox week1day1;

    @FXML
    private Button btnRemoveAppointment;

    @FXML
    private VBox week1day6;

    @FXML
    private VBox week1day5;

    @FXML
    private VBox week1day4;

    @FXML
    private VBox week1day3;

    @FXML
    private Button btnRemoveContact;

    @FXML
    private VBox week1day7;

    @FXML
    private WebView viewEventInfo;

    @FXML
    private Button btnNewContact;

    @FXML
    private Button btnEditAppointment;

    @FXML
    private Button btnEditContact;

    @FXML
    private VBox week4day5;

    @FXML
    private VBox week4day4;

    @FXML
    private VBox week4day7;

    @FXML
    private VBox week4day6;

    @FXML
    private VBox week4day1;

    @FXML
    private MenuItem mnuProfile;

    @FXML
    private ToggleButton btnWeekView;

    @FXML
    private VBox week4day3;

    @FXML
    private VBox week4day2;

    @FXML
    private VBox week2day1;

    @FXML
    private MenuItem mnuEdit;

    @FXML
    private VBox week2day3;

    @FXML
    private GridPane gridMonthView;

    @FXML
    private VBox week2day2;

    @FXML
    private VBox week3day7;

    @FXML
    private MenuItem mnuRemove;

    @FXML
    private VBox week2day5;

    @FXML
    private VBox week3day6;

    @FXML
    private VBox week2day4;

    @FXML
    private VBox week3day5;

    @FXML
    private ToggleGroup grpView;

    @FXML
    private VBox week2day7;

    @FXML
    private VBox week3day4;

    @FXML
    private VBox week2day6;

    @FXML
    private VBox week3day3;

    @FXML
    private VBox week3day2;

    @FXML
    private VBox week3day1;

    @FXML
    private ToggleButton btnMonthView;

    @FXML
    private Button btnNewAppointment;

    @FXML
    private TextField txtSearchContacts;

    @FXML
    private VBox week5day7;

    @FXML
    private VBox week5day3;

    @FXML
    private VBox week5day4;

    @FXML
    private WebView webView;

    @FXML
    private VBox week5day5;

    @FXML
    private VBox week5day6;

    @FXML
    private MenuItem mnuCreate;

    @FXML
    private VBox week5day1;

    @FXML
    private Label lblMonthName;

    @FXML
    private VBox week5day2;
    
    @FXML
    private ArrayList<VBox> listMonthViewDays;
    
    @FXML
    private SubScene sceneDayView;
    
    @FXML
    private StackPane stackPane;
    
    @FXML
    private GridPane gridWeekView;
    
    @FXML
    private MenuButton mnuReports;
    
    @FXML
    private MenuButton btnTimezones;
    
    @FXML
    private Label lblTimezone;
    
    @FXML
    private VBox vbxNotificationArea;
    
    @FXML
    private TabPane tabPane;
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        selectedLang = rb;
        lstContacts.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        lstContacts.getSelectionModel().selectedItemProperty().addListener(this);
        datePicker.setValue(LocalDate.now());
        
        try{
            FXMLLoader loader = new FXMLLoader(getClass().getResource("DayView.fxml"), rb);
            loader.load();
            Scene scene = new Scene(loader.getRoot());
            stackPane.getChildren().add(scene.getRoot());

            dayViewController = loader.getController();
            dayViewController.setMainWindowController(this);
            dayViewController.setScene(scene);
        } catch (IOException ex) {
            Logger.getLogger(MainWindowController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try{
            FXMLLoader loader = new FXMLLoader(getClass().getResource("WeekView.fxml"), rb);
            loader.load();
            Scene scene = new Scene(loader.getRoot());
            stackPane.getChildren().add(scene.getRoot());

            weekViewController = loader.getController();
            weekViewController.setMainWindowController(this);
            weekViewController.setScene(scene);
        } catch (IOException ex) {
            Logger.getLogger(MainWindowController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        switchToMonthView(null);
        
        notificationsUpdaterExecutor = Executors.newScheduledThreadPool(1);
        Runnable task = () -> {
            try{
                updateNotifications();
            }
            catch(Exception e){
                e.printStackTrace();
            }
        };
        notificationsUpdaterExecutor.scheduleWithFixedDelay(task, 0, 1, TimeUnit.MINUTES);
    }   

    /**
     * Checks the database for appointments starting within the next 15 minutes
     * and adds a notification to the top of the window for them.
     */
    public void updateNotifications(){
        if(databaseManager != null){
            ArrayList<Appointment> appointments = databaseManager.getAppointmentsBetween(LocalDateTime.now(), LocalDateTime.now().plusMinutes(15));
            for(Appointment appt : appointments){
                if(!notificationAlreadyShown.contains(appt)){
                    BorderPane bp = new BorderPane();
                    bp.setOnMouseEntered(e -> bp.setStyle("-fx-background-color: yellow;"));
                    bp.setOnMouseExited(e -> bp.setStyle("-fx-background-color: lightyellow;"));
                    
                    Text t1 = new Text(selectedLang.getString("appointmentStarting") + ":  ");
                    Text t2 = new Text(appt.getStart().toLocalTime().format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT)) + " \u21d2 " + appt.getTitle());
                    t2.setFont(Font.font(t2.getFont().getName(), FontWeight.BOLD, t2.getFont().getSize()));
                    TextFlow lbl = new TextFlow();
                    lbl.setTextAlignment(TextAlignment.CENTER);
                    lbl.getChildren().addAll(t1, t2);
                    Hyperlink lnk = new Hyperlink("\u2716");
                    lnk.setOnAction(e -> vbxNotificationArea.getChildren().remove(bp));
                    bp.setCenter(lbl);
                    bp.setRight(lnk);
                    Platform.runLater(() -> {
                        vbxNotificationArea.getChildren().add(bp);
                    });
                    notificationAlreadyShown.add(appt);
                }
            }
            for(Appointment appt : notificationAlreadyShown){
                if(appt.getStart().isBefore(LocalDateTime.now())){
                    notificationAlreadyShown.remove(appt);
                }
            }
        }
    }
    
    public void populateTimezoneList(){
        HashMap<String, ArrayList<TimeZoneHelper>> timezones = new HashMap<>();
        ArrayList<TimeZoneHelper> regionless = new ArrayList<>();
        for(String zone : TimeZone.getAvailableIDs()){
            TimeZoneHelper tzh = new TimeZoneHelper(zone);
            if(tzh.hasRegion()){
                if(!timezones.containsKey(tzh.getRegion())){
                    timezones.put(tzh.getRegion(), new ArrayList<TimeZoneHelper>());
                }
                timezones.get(tzh.getRegion()).add(tzh);
            }
            else{
                regionless.add(tzh);
            }
        }
        for(Map.Entry<String, ArrayList<TimeZoneHelper>> entry : timezones.entrySet()){
            String region = entry.getKey();
            ArrayList<TimeZoneHelper> zones = entry.getValue();
            Menu regionMenu = new Menu(region);
            for(TimeZoneHelper zone : zones){
                MenuItem mnuZone = new MenuItem(zone.getOffsetString() + " " + zone.getName());
                mnuZone.setUserData(zone);
                mnuZone.setOnAction(e -> {
                    TimeZone.setDefault(((TimeZoneHelper)((MenuItem)e.getSource()).getUserData()).getTimeZone());
                    lblTimezone.setText(((MenuItem)e.getSource()).getText());
                    lblTimezone.setUserData(((MenuItem)e.getSource()).getUserData());
                    refreshCalendars();
                });
                regionMenu.getItems().add(mnuZone);
            }
            btnTimezones.getItems().add(regionMenu);
        }
        String dst = selectedLang.getString("dst");
        for(TimeZoneHelper zone : regionless){
            String dstSavings = (zone.getTimeZone().getDSTSavings() > 0 ? "+" : "") + Integer.toString(zone.getTimeZone().getDSTSavings() / 3600000);
            MenuItem mnuZone = new MenuItem(zone.getOffsetString() + " " + zone.getName() + (zone.getTimeZone().useDaylightTime() ? " (" + dst + " " + dstSavings + ")" : ""));
            mnuZone.setUserData(zone);
            mnuZone.setOnAction(e -> {
                TimeZone.setDefault(((TimeZoneHelper)((MenuItem)e.getSource()).getUserData()).getTimeZone());
                lblTimezone.setText(((MenuItem)e.getSource()).getText());
                lblTimezone.setUserData(((MenuItem)e.getSource()).getUserData());
                refreshCalendars();
            });
            btnTimezones.getItems().add(mnuZone);
        }
        TimeZoneHelper hereZone = new TimeZoneHelper(TimeZone.getDefault().getID());
        String dstSavings = (hereZone.getTimeZone().getDSTSavings() > 0 ? "+" : "") + Integer.toString(hereZone.getTimeZone().getDSTSavings() / 3600000);
        lblTimezone.setText(hereZone.getOffsetString() + " " + hereZone.getName() + (hereZone.getTimeZone().useDaylightTime() ? " (" + dst + " " + dstSavings + ")" : ""));
        GridPane.setFillHeight(btnTimezones, true);
    }
    
    @FXML
    private void switchToMonthView(ActionEvent e){
        gridMonthView.toFront();
        btnMonthView.setSelected(true);
    }
    
    @FXML
    private void switchToWeekView(ActionEvent e){
        weekViewController.getScene().getRoot().toFront();
        btnWeekView.setSelected(true);
    }
    
    @FXML
    private void switchToDayView(ActionEvent e){
        dayViewController.getScene().getRoot().toFront();
        btnDayView.setSelected(true);
    }
    
    @FXML
    private void previousMonth(ActionEvent e){
        datePicker.setValue(datePicker.getValue().minusMonths(1));
    }
    
    @FXML
    private void nextMonth(ActionEvent e){
        datePicker.setValue(datePicker.getValue().plusMonths(1));
    }
    
    @FXML
    private void searchContactsChanged(KeyEvent e){
        searchCustomers(txtSearchContacts.getText());
    }
    
    /**
     * Searches for customers LIKE what the user typed, sorted ascending.
     * @param text
     */
    public void searchCustomers(String text){
        searchCustomers(text, DatabaseManager.Sort.ASC);
    }

    /**
     * Searches for customers LIKE what the user typed, sorted as specified.
     * @param text
     * @param sort
     */
    public void searchCustomers(String text, DatabaseManager.Sort sort){
        ArrayList<Customer> customers = databaseManager.getCustomersByCustomername(text, sort);
        lstContacts.setItems(FXCollections.observableArrayList(customers));
    }
    
    /**
     * Generates the week report for the user and opens it in a new tab.
     * @param e
     */
    @FXML
    public void generateWeekReport(ActionEvent e){
        String report = loadReport("week_schedule");
        Pattern pattern = Pattern.compile("<template id=\"day-title\">(.*?)<\\/template>", Pattern.MULTILINE | Pattern.DOTALL);
        Matcher matcher = pattern.matcher(report);
        matcher.find();
        String dayTitle = matcher.group(1);
        pattern = Pattern.compile("<template id=\"appointment-template\">(.*?)<\\/template>", Pattern.MULTILINE | Pattern.DOTALL);
        matcher = pattern.matcher(report);
        matcher.find();
        String template = matcher.group(1);
        String insert = "";
        
        LocalDate date = datePicker.getValue();
        int dayOfWeek = date.getDayOfWeek().getValue();
        dayOfWeek = dayOfWeek == 7 ? 0 : dayOfWeek;
        LocalDate firstDay = date.minusDays(dayOfWeek);
        LocalDate lastDay = firstDay.plusDays(6);
        
        report = report.replace("$firstday$", firstDay.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)));
        String to = " " + selectedLang.getString("to") + " ";
        report = report.replace("$periodString$", firstDay.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)) + to + lastDay.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)));
        
        for(int i = 0; i < 7; i++){
            LocalDate day = firstDay.plusDays(i);
            insert += dayTitle.replace("$date$", day.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL))) + "\n";
            
            ArrayList<Appointment> appointments = databaseManager.getAppointmentsOnDate(day);
            if(appointments.isEmpty()){
                insert += "<p><i>" + selectedLang.getString("noAppointments") + "</i></p>";
            }
            for(Appointment appt : appointments){
                String copy = new String(template);
                copy = copy.replace("$title$", appt.getTitle()== null ? "" : appt.getTitle());
                copy = copy.replace("$description$", appt.getDescription()== null ? "" : appt.getDescription());
                copy = copy.replace("$customerName$", appt.getCustomer().getCustomerName()== null ? "" : appt.getCustomer().getCustomerName());
                copy = copy.replace("$location$", appt.getLocation() == null ? "" : appt.getLocation());
                copy = copy.replace("$contact$", appt.getContact()== null ? "" : appt.getContact());
                copy = copy.replace("$type$", appt.getType()== null ? "" : appt.getType());
                copy = copy.replace("$url$", appt.getUrl()== null ? "" : appt.getUrl());
                copy = copy.replace("$start$", appt.getStart().format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT)));
                copy = copy.replace("$end$", appt.getEnd().format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT)));
                copy = copy.replace("$duration$", appt.getDurationString());
                copy = copy.replace("$createdBy$", appt.getCreatedBy()== null ? "" : appt.getCreatedBy());
                copy = copy.replace("$createDate$", appt.getCreateDate().format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)));
                copy = copy.replace("$lastUpdateBy$", appt.getLastUpdateBy()== null ? "" : appt.getLastUpdateBy());
                copy = copy.replace("$lastUpdate$", appt.getLastUpdate().format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)));
                insert += copy;
            }
        }
        report = report.replace("$schedule$", insert);
        showReport(report);
    }
    
    /**
     * Generates the agenda report for the user and opens it in a new tab.
     * @param e
     */
    @FXML
    public void generateAgendaReport(ActionEvent e){
        String report = loadReport("agenda_schedule");
        Pattern pattern = Pattern.compile("<template id=\"appointment-template\">(.*?)<\\/template>", Pattern.MULTILINE | Pattern.DOTALL);
        Matcher matcher = pattern.matcher(report);
        matcher.find();
        String template = matcher.group(1);
        String insert = "";
        
        LocalDate date = datePicker.getValue();
        
        report = report.replace("$date$", date.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)));
            
        ArrayList<Appointment> appointments = databaseManager.getAppointmentsOnDate(date);
        if(appointments.isEmpty()){
            insert += "<p><i>" + selectedLang.getString("noAppointments") + "</i></p>";
        }
        for(Appointment appt : appointments){
            String copy = new String(template);
            copy = copy.replace("$title$", appt.getTitle()== null ? "" : appt.getTitle());
            copy = copy.replace("$description$", appt.getDescription()== null ? "" : appt.getDescription());
            copy = copy.replace("$customerName$", appt.getCustomer().getCustomerName()== null ? "" : appt.getCustomer().getCustomerName());
            copy = copy.replace("$location$", appt.getLocation() == null ? "" : appt.getLocation());
            copy = copy.replace("$contact$", appt.getContact()== null ? "" : appt.getContact());
            copy = copy.replace("$type$", appt.getType()== null ? "" : appt.getType());
            copy = copy.replace("$url$", appt.getUrl()== null ? "" : appt.getUrl());
            copy = copy.replace("$start$", appt.getStart().format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT)));
            copy = copy.replace("$end$", appt.getEnd().format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT)));
            copy = copy.replace("$duration$", appt.getDurationString());
            copy = copy.replace("$createdBy$", appt.getCreatedBy()== null ? "" : appt.getCreatedBy());
            copy = copy.replace("$createDate$", appt.getCreateDate().format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)));
            copy = copy.replace("$lastUpdateBy$", appt.getLastUpdateBy()== null ? "" : appt.getLastUpdateBy());
            copy = copy.replace("$lastUpdate$", appt.getLastUpdate().format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)));
            insert += copy;
        }
        report = report.replace("$schedule$", insert);
        showReport(report);
    }
    
    /**
     * Generates the month appointment types report for the user and opens it in a new tab.
     * @param e
     */
    @FXML
    public void generateTypeReport(ActionEvent e){
        String report = loadReport("appt_types");
        Pattern pattern = Pattern.compile("<template id=\"type-template\">(.*?)<\\/template>", Pattern.MULTILINE | Pattern.DOTALL);
        Matcher matcher = pattern.matcher(report);
        matcher.find();
        String template = matcher.group(1);
        String insert = "";
        LocalDate date = datePicker.getValue();
        LocalDate firstDay = date.withDayOfMonth(1);
        LocalDate lastDay = date.withDayOfMonth(date.getMonth().length(date.isLeapYear()));
        int total = databaseManager.getAppointmentTotal(loggedInUser, firstDay.atStartOfDay(), lastDay.atTime(23, 59, 59));
        report = report.replace("$date$", date.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)));
        report = report.replace("$month$", date.getMonth().getDisplayName(TextStyle.FULL, Locale.getDefault()));
        report = report.replace("$periodString$", firstDay.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)) + " to " + lastDay.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)));
        report = report.replace("$total$", Integer.toString(total));            
        ArrayList<String> types = databaseManager.getAppointmentTypes(DatabaseManager.Sort.ASC);
        types.remove(null);
        if(!types.contains(""))types.add("");
        if(types.isEmpty() && total > 0){
            insert += "<p><i>" + selectedLang.getString("noAppointmentType") + "</i></p>";
        }
        else if(total == 0){
            insert += "<p><i>" + selectedLang.getString("noAppointmentsForDate") + "</i></p>";
        }
        for(String type : types){
            if(type != null){
                int count = databaseManager.getAppointmentTypeCount(type, loggedInUser, firstDay.atStartOfDay(), lastDay.atTime(23, 59, 59));
                int percent = (int)Math.round(((double)count / (double)types.size()) * 100);
                String copy = new String(template);
                copy = copy.replace("$type$", type.trim().equals("") ? "(" + selectedLang.getString("noTypeSpecified") + ")" : type);
                copy = copy.replace("$count$", Integer.toString(count));
                copy = copy.replace("$total$", Integer.toString(total));
                copy = copy.replace("$percent$", Integer.toString(percent));
                insert += copy;
            }
        }
        report = report.replace("$summary$", insert);
        showReport(report);
    }
    
    /**
     * Loads the specified report from it's HTML file, sensitive to the
     * application's language.
     * @param reportFile
     * @return 
     */
    private String loadReport(String reportFile){
        reportFile += "_" + Locale.getDefault().getLanguage() + ".html";
        InputStream streamer = getClass().getResourceAsStream("/reports/" + reportFile);
        return convertStreamToString(streamer);
    }
    
    /**
     * Loads the CSS to style the report and then opens the report in a new tab
     * with a print button.
     * @param report 
     */
    private void showReport(String report){
        InputStream streamer = getClass().getResourceAsStream("/reports/report_styles.css");
        String css = convertStreamToString(streamer);
        report = report.replace("<style></style>", "<style>\n" + css + "\n</style>");
        report = report.replace("$username$", this.loggedInUser.getUserName());
        Pattern pattern = Pattern.compile("<title>(.*)<\\/title>", Pattern.MULTILINE | Pattern.DOTALL);
        Matcher matcher = pattern.matcher(report);
        matcher.find();
        String title = matcher.group(1);
        
        BorderPane bp = new BorderPane();
        WebView wv = new WebView();
        bp.setCenter(wv);
        Button btnPrint = new Button();
        ImageView imgPrint = null;
        try {
            imgPrint = new ImageView(new Image(getClass().getResource("/img/print-solid-24x24.png").toURI().toString()));
        } catch (URISyntaxException ex) {
            Logger.getLogger(MainWindowController.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(imgPrint == null){
            btnPrint.setText(selectedLang.getString("print"));
        }
        else{
            btnPrint.setGraphic(imgPrint);
        }
        btnPrint.setTooltip(new Tooltip(selectedLang.getString("printReport")));
        FlowPane fp = new FlowPane();
        fp.getChildren().add(btnPrint);
        fp.setPadding(new Insets(10));
        bp.setTop(fp);
        Tab tab = new Tab(title, bp);
        wv.getEngine().loadContent(report);
        tab.setClosable(true);
        tabPane.getTabs().add(tab);
        tabPane.getSelectionModel().select(tab);
        btnPrint.setOnAction(e -> {
            PrinterJob job = PrinterJob.createPrinterJob();
            if(job != null){
                if(job.showPrintDialog(this.myStage)){
                    wv.getEngine().print(job);
                }
            }
        });
    }
    
    /**
     * Gets the contents of a resource file using the InputStream and returns
     * it as a string.
     * Borrowed from Pavel Repin here: https://stackoverflow.com/questions/309424/how-do-i-read-convert-an-inputstream-into-a-string-in-java
     * @param is
     * @return 
     */
    public static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }
    
    /**
     * Loads the customer list and attempts to reselect the customers that the
     * user had selected before.
     */
    public void refreshList(){
        ObservableList<Customer> selected = lstContacts.getSelectionModel().getSelectedItems();
        searchCustomers(txtSearchContacts.getText());
        for(Customer customer : lstContacts.getItems()){
            for(Customer selectedCustomer : selected){
                if(customer.getCustomerId() == selectedCustomer.getCustomerId()){
                    lstContacts.getSelectionModel().select(customer); 
                }
            }
        }
    }
    
    @FXML
    void newContactClicked(ActionEvent event) {
        editCustomer(databaseManager.newCustomer());
    }

    @FXML
    void editContactClicked(ActionEvent event) {
        int editDialogWidth = 600;
        int editDialogHeight = 400;
        int x = (int)Math.round(myStage.getX() + myStage.getWidth() / 2 - editDialogWidth / 2);
        int y = (int)Math.round(myStage.getY() + myStage.getHeight() / 2 - editDialogHeight / 2);
        int step = new Double(myStage.getScene().getY()).intValue(); // new Double(myStage.getScene().getWindow().getHeight() - myStage.getScene().getHeight() - myStage.getScene().getY()).intValue();
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        ObservableList<City> cities = FXCollections.observableArrayList(databaseManager.getAllCities(DatabaseManager.Sort.ASC));
        ObservableList<Country> countries = FXCollections.observableArrayList(databaseManager.getAllCountries(DatabaseManager.Sort.ASC));
        for(Customer customer : lstContacts.getSelectionModel().getSelectedItems()){
            editCustomer(customer, x, y, cities, countries);
            x += step;
            y += step;
            if(x + editDialogWidth > screenSize.getWidth()){
                x = 0;
            }
            if(y + editDialogHeight > screenSize.getHeight()){
                y = 0;
            }
        }
    }
    
    @FXML
    void editUserProfile(ActionEvent e){
        Stage editorStage = new Stage();
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("EditUserDialog.fxml"), selectedLang);
            loader.load();
            Scene scene = new Scene(loader.getRoot());
            editorStage.setTitle(selectedLang.getString("editUser") + " - " + (this.loggedInUser.getUserName() == null ? selectedLang.getString("newUser") : this.loggedInUser.getUserName()));
            editorStage.setScene(scene);
            editorStage.initOwner(myStage);
            editorStage.initModality(Modality.WINDOW_MODAL);
            editorStage.show();
            
            EditUserDialogController editorController = loader.getController();
            editorController.setStage(editorStage);
            editorController.setUser(this.loggedInUser);
            editorController.setMainWindowController(this);
        } catch (IOException ex) {
            DialogPopper.showError(selectedLang.getString("apptitle"), selectedLang.getString("error"), selectedLang.getString("errorEditUserDialog"), this.myStage, selectedLang);
            Logger.getLogger(AppointmentCalendar.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @FXML
    void logout(){
        writeToLog("User Logout: " + loggedInUser.getUserName() + "(" + loggedInUser.getUserId() + ")");
        this.myAppointmentCalendarApp.switchToLoginDialog();
    }

    @FXML
    void removeAppointmentClicked(ActionEvent e){
        removeAppointment();
    }
    
    /**
     * Deletes the selected appointment.
     */
    public void removeAppointment(){
        if(this.selectedAppointment != null){
            if(DialogPopper.showYesNo(selectedLang.getString("apptitle"), selectedLang.getString("removeAppointment") + "?", selectedLang.getString("removeAppointmentMessage").replace("$title$", this.selectedAppointment.getTitle()), myStage, selectedLang)){
                selectedAppointment.delete();
                refreshCalendars();
            }
        }
    }
    
    @FXML
    void removeContactClicked(ActionEvent event) {
        removeContacts();
    }
    
    /**
     * Deletes the selected contacts.
     */
    public void removeContacts(){
        if(lstContacts.getSelectionModel().getSelectedItems().size() > 0){
            String users = "";
            String comma = "";
            for(Customer customer : lstContacts.getSelectionModel().getSelectedItems()){
                users += comma + customer.getCustomerName();
                comma = ", ";
            }
            if(DialogPopper.showYesNo(selectedLang.getString("apptitle"), selectedLang.getString("removeUsers"), users, myStage, selectedLang)){
                for(Customer customer : lstContacts.getSelectionModel().getSelectedItems()){
                    customer.delete();
                }
                refreshList();
                updateCustomerDisplay();
            }
        }
    }
    
    /**
     * Opens a dialog box to edit this customer.
     * @param customer
     */
    public void editCustomer(Customer customer){
        ObservableList<City> cities = FXCollections.observableArrayList(databaseManager.getAllCities(DatabaseManager.Sort.ASC));
        ObservableList<Country> countries = FXCollections.observableArrayList(databaseManager.getAllCountries(DatabaseManager.Sort.ASC));
        editCustomer(customer, -1, -1, cities, countries);
    }

    /**
     * Opens a dialog box to edit this customer at the specified coordinates.
     * @param customer
     * @param x
     * @param y
     * @param cities
     * @param countries
     */
    public void editCustomer(Customer customer, int x, int y, ObservableList<City> cities, ObservableList<Country> countries){
        Stage editorStage = new Stage();
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("EditCustomerDialog.fxml"), selectedLang);
            loader.load();
            Scene scene = new Scene(loader.getRoot());
            editorStage.setTitle(selectedLang.getString("editCustomer") + " - " + (customer.getCustomerName() == null ? selectedLang.getString("newCustomer") : customer.getCustomerName()));
            editorStage.setScene(scene);
            editorStage.initOwner(myStage);
            editorStage.initModality(Modality.WINDOW_MODAL);
            if(x >= 0){
                editorStage.setX(x);
            }
            if(y >= 0){
                editorStage.setY(y);
            }
            editorStage.show();
            
            EditCustomerDialogController editorController = loader.getController();
            editorController.setStage(editorStage);
            editorController.setCustomer(customer);
            editorController.setMainWindowController(this);
            editorController.setCitiesList(cities);
            editorController.setCountriesList(countries);
        } catch (IOException ex) {
            DialogPopper.showError(selectedLang.getString("apptitle"), selectedLang.getString("error"), selectedLang.getString("errorEditCustomerDialog"), this.myStage, selectedLang);
            Logger.getLogger(AppointmentCalendar.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @FXML
    void newAppointmentClicked(ActionEvent e){
        Appointment newAppt = this.databaseManager.newAppointment();
        newAppt.setStart(LocalDateTime.of(datePicker.getValue(), LocalTime.now()));
        newAppt.setEnd(LocalDateTime.of(datePicker.getValue(), LocalTime.now().plusMinutes(30)));
        editAppointment(newAppt);
    }
    
    @FXML
    void editAppointmentClicked(ActionEvent e){
        if(this.selectedAppointment != null){
            editAppointment(this.selectedAppointment);
        }
    }
    
    /**
     * Opens a dialog to edit the appointment.
     * @param appointment
     */
    public void editAppointment(Appointment appointment){
        Stage editorStage = new Stage();
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("EditAppointmentDialog.fxml"), selectedLang);
            loader.load();
            Scene scene = new Scene(loader.getRoot());
            editorStage.setTitle(selectedLang.getString("editAppointment") + " - " + (appointment.getTitle() == null ? selectedLang.getString("newAppointment") : appointment.getTitle()));
            editorStage.setScene(scene);
            editorStage.initOwner(myStage);
            editorStage.initModality(Modality.WINDOW_MODAL);
            editorStage.show();
            
            EditAppointmentDialogController editorController = loader.getController();
            editorController.setStage(editorStage);
            editorController.setAppointment(appointment);
            editorController.setMainWindowController(this);
            editorController.setCustomersList(FXCollections.observableArrayList(this.databaseManager.getAllCustomers(DatabaseManager.Sort.ASC)));
            editorController.setTypeList(FXCollections.observableArrayList(this.databaseManager.getAppointmentTypes(DatabaseManager.Sort.ASC)));
        } catch (IOException ex) {
            DialogPopper.showError(selectedLang.getString("apptitle"), selectedLang.getString("error"), selectedLang.getString("errorEditAppointmentDialog"), this.myStage, selectedLang);
            Logger.getLogger(AppointmentCalendar.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @FXML
    void datePicked(ActionEvent e){
        refreshCalendars();
    }
    
    /**
     * Refreshes the data on all three of the calendar views.
     */
    public void refreshCalendars(){
        refreshDayView();
        refreshWeekView();
        refreshMonthView();
        LocalDate date = datePicker.getValue();
        ArrayList<Appointment> appointments = databaseManager.getAppointmentsOnDate(date);
        updateInfoPanel(appointments, date);
    }
    
    /**
     * Sets a new date for the day view and reloads the appointments.
     */
    public void refreshDayView(){
        this.dayViewController.setDate(datePicker.getValue());
    }
    
    /**
     * Sets a new date for the week view and reloads the appointments.
     */
    public void refreshWeekView(){
        this.weekViewController.setDate(datePicker.getValue());
    }
    
    /**
     * Sets a new date for the month view and reloads the appointments.
     */
    public void refreshMonthView(){
        lblDay1.setText(DayOfWeek.SUNDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()));
        lblDay2.setText(DayOfWeek.MONDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()));
        lblDay3.setText(DayOfWeek.TUESDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()));
        lblDay4.setText(DayOfWeek.WEDNESDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()));
        lblDay5.setText(DayOfWeek.THURSDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()));
        lblDay6.setText(DayOfWeek.FRIDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()));
        lblDay7.setText(DayOfWeek.SATURDAY.getDisplayName(TextStyle.FULL, Locale.getDefault()));
        
        LocalDate selectedDate = datePicker.getValue();
        LocalDate firstOfMonth = selectedDate.withDayOfMonth(1);
        int daysInMonth = selectedDate.lengthOfMonth();
        int firstDayOfWeek = firstOfMonth.getDayOfWeek().getValue() - 1;
        
        lblMonthName.setText(selectedDate.getMonth().getDisplayName(TextStyle.FULL, Locale.getDefault()));
        
        for(int i = 1; i <= daysInMonth; i++){
            LocalDate date = firstOfMonth.withDayOfMonth(i);
            VBox vb = listMonthViewDays.get(firstDayOfWeek + i);
            vb.getChildren().clear();
            Hyperlink day = new Hyperlink(Integer.toString(i));
            day.setFont(Font.font(day.getFont().getFamily(), FontWeight.BOLD, day.getFont().getSize()));
            //Using lambda expression to keep code concise and avoid instantiating extra objects.
            day.setOnAction(e -> {
                Hyperlink d = (Hyperlink)e.getSource();
                LocalDate clickedDay = firstOfMonth.withDayOfMonth(Integer.parseInt(d.getText()));
                datePicker.setValue(clickedDay);
                ArrayList<Appointment> appointments = databaseManager.getAppointmentsOnDate(date);
                updateInfoPanel(appointments, date);
            });
            HBox aligner = new HBox();
            aligner.setAlignment(Pos.CENTER_RIGHT);
            aligner.getChildren().add(day);
            aligner.setBackground(new Background(new BackgroundFill(Color.LIGHTBLUE, CornerRadii.EMPTY, Insets.EMPTY)));
            if(i == selectedDate.getDayOfMonth()){
                vb.setStyle("-fx-border-color: blue; -fx-background-color: azure;");
            }
            else{
                vb.setStyle("-fx-border-color: gray");
            }
            vb.getChildren().add(aligner);
            ArrayList<Appointment> appointments = databaseManager.getAppointmentsOnDate(date);
            for(Appointment appt : appointments){
                AppointmentHyperlink apptLink = new AppointmentHyperlink(appt);
                apptLink.setOnAction(e -> {
                    AppointmentHyperlink l = (AppointmentHyperlink)e.getSource();
                    l.setStyle("-fx-font-weight: bold; -fx-background-color: yellow;");
                    updateInfoPanel(l.getAppointment(), date);
                    this.setSelectedAppointment(l.getAppointment());
                });
                vb.getChildren().add(apptLink);
            }
        }
        
        LocalDate lastMonth = selectedDate.minusMonths(1);
        int daysInLastMonth = lastMonth.lengthOfMonth();
        
        for(int i = firstDayOfWeek; i >= 0; i--){
            int dayOfMonth = daysInLastMonth - (firstDayOfWeek - i);
            LocalDate date = lastMonth.withDayOfMonth(dayOfMonth);
            VBox vb = listMonthViewDays.get(i);
            vb.getChildren().clear();
            Hyperlink day = new Hyperlink(Integer.toString(dayOfMonth));
            //Using lambda expression to keep code concise and avoid instantiating extra objects.
            day.setOnAction(e -> {
                Hyperlink d = (Hyperlink)e.getSource();
                datePicker.setValue(lastMonth.withDayOfMonth(Integer.parseInt(d.getText())));
            });
            HBox aligner = new HBox();
            aligner.setAlignment(Pos.CENTER_RIGHT);
            aligner.getChildren().add(day);
            aligner.setBackground(new Background(new BackgroundFill(Color.LIGHTGRAY, CornerRadii.EMPTY, Insets.EMPTY)));
            vb.getChildren().add(aligner);
            ArrayList<Appointment> appointments = databaseManager.getAppointmentsOnDate(date);
            for(Appointment appt : appointments){
                AppointmentHyperlink apptLink = new AppointmentHyperlink(appt);
                apptLink.setOnAction(e -> {
                    AppointmentHyperlink l = (AppointmentHyperlink)e.getSource();
                    l.setStyle("-fx-font-weight: bold; -fx-background-color: yellow;");
                    updateInfoPanel(l.getAppointment(), date);
                    this.setSelectedAppointment(l.getAppointment());
                });
                vb.getChildren().add(apptLink);
            }
        }
        
        LocalDate nextMonth = selectedDate.plusMonths(1);
        int daysInNextMonth = nextMonth.lengthOfMonth();
        
        for(int i = (41 - firstDayOfWeek - daysInMonth); i > 0; i--){
            LocalDate date = nextMonth.withDayOfMonth(i);
            VBox vb = listMonthViewDays.get(i + firstDayOfWeek + daysInMonth);
            vb.getChildren().clear();
            Hyperlink day = new Hyperlink(Integer.toString(i));
            //Using lambda expression to keep code concise and avoid instantiating extra objects.
            day.setOnAction(e -> {
                Hyperlink d = (Hyperlink)e.getSource();
                datePicker.setValue(nextMonth.withDayOfMonth(Integer.parseInt(d.getText())));
            });
            HBox aligner = new HBox();
            aligner.setAlignment(Pos.CENTER_RIGHT);
            aligner.getChildren().add(day);
            aligner.setBackground(new Background(new BackgroundFill(Color.LIGHTGRAY, CornerRadii.EMPTY, Insets.EMPTY)));
            vb.getChildren().add(aligner);
            ArrayList<Appointment> appointments = databaseManager.getAppointmentsOnDate(date);
            for(Appointment appt : appointments){
                AppointmentHyperlink apptLink = new AppointmentHyperlink(appt);
                apptLink.setOnAction(e -> {
                    AppointmentHyperlink l = (AppointmentHyperlink)e.getSource();
                    l.setStyle("-fx-font-weight: bold; -fx-background-color: yellow;");
                    updateInfoPanel(l.getAppointment(), date);
                    this.setSelectedAppointment(l.getAppointment());
                });
                vb.getChildren().add(apptLink);
            }
        }  
        
    }
    
    /**
     * Updates the information panel to show the appointment's data.
     * @param appointment
     * @param date
     */
    public void updateInfoPanel(Appointment appointment, LocalDate date){
        ArrayList<Appointment> one = new ArrayList<>();
        one.add(appointment);
        updateInfoPanel(one, date);
    }

    /**
     * Updates the information panel to show the appointments' data.
     * @param appointments
     * @param date
     */
    public void updateInfoPanel(ArrayList<Appointment> appointments, LocalDate date){
        LocalTime open = databaseManager.getOpenTime(date.getDayOfWeek());
        LocalTime close = databaseManager.getCloseTime(date.getDayOfWeek());
        String businessHours = "Closed";
        if(open != null && close != null){
            businessHours = open.format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT)) + " to " + close.format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT));
        }
        String html = "<!DOCTYPE html>\n" +
            "<html>\n" +
            "    <head>\n" +
            "        <title>" + selectedLang.getString("appointmentView") + "</title>\n" +
            "        <meta charset=\"UTF-8\">\n" +
            "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
            "    </head>\n" +
            "    <body>\n" +
            "       <h1>" + date.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG)) + "</h1>\n" +
            "       <h3>" + date.getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.getDefault()) + "</h3>" +
            "       " + selectedLang.getString("businessHours") + ": " + businessHours +
            "       <hr>\n";
        String template = "<div>\n" +
            "    <h2>$title</h2>\n" +
            "    <p>$description</p>\n" +
            "    <p><b>" + selectedLang.getString("withClient") + ":</b> $customerName<br>\n" +
            "    <p><b>" + selectedLang.getString("location") + ":</b> $location<br>\n" +
            "    <p><b>" + selectedLang.getString("contact") + ":</b> $contact<br>\n" +
            "    <p><b>" + selectedLang.getString("appointmentType") + ":</b> $type<br>\n" +
            "    <p><b>" + selectedLang.getString("webAddress") + ":</b> $url<br>\n" +
            "    <p>" + selectedLang.getString("from") + " $start " + selectedLang.getString("to") + " $end ($duration).</p>\n" +
            "    <p><small><i>" + selectedLang.getString("createdByString") + "</i></small>\n" +
            "</div>";
        for(Appointment appt : appointments){
            String copy = new String(template);
            copy = copy.replace("$title", appt.getTitle()== null ? "" : appt.getTitle());
            copy = copy.replace("$description", appt.getDescription()== null ? "" : appt.getDescription());
            copy = copy.replace("$customerName", appt.getCustomer().getCustomerName()== null ? "" : appt.getCustomer().getCustomerName());
            copy = copy.replace("$location", appt.getLocation() == null ? "" : appt.getLocation());
            copy = copy.replace("$contact", appt.getContact()== null ? "" : appt.getContact());
            copy = copy.replace("$type", appt.getType()== null ? "" : appt.getType());
            copy = copy.replace("$url", appt.getUrl()== null ? "" : appt.getUrl());
            copy = copy.replace("$start", appt.getStart().format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT)));
            copy = copy.replace("$end", appt.getEnd().format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT)));
            copy = copy.replace("$duration", appt.getDurationString());
            copy = copy.replace("$createdBy", appt.getCreatedBy()== null ? "" : appt.getCreatedBy());
            copy = copy.replace("$createDate", appt.getCreateDate().format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)));
            copy = copy.replace("$lastUpdateBy", appt.getLastUpdateBy()== null ? "" : appt.getLastUpdateBy());
            copy = copy.replace("$lastUpdate", appt.getLastUpdate().format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)));
            html += copy;
            html += "<hr>";
        }
        html += "    </body>\n</html>";
        viewEventInfo.getEngine().loadContent(html);
    }
    
    /**
     * Returns the stage associated with this controller.
     * @return
     */
    public Stage getStage() {
        return myStage;
    }

    /**
     * Sets the stage associated with this controller.
     * @param myStage
     */
    public void setStage(Stage myStage) {
        this.myStage = myStage;
        myStage.setOnCloseRequest(e -> {
            if(DialogPopper.showYesNo(myAppointmentCalendarApp.getLangSelected().getString("apptitle"), myAppointmentCalendarApp.getLangSelected().getString("questionExit"), "", myStage, myAppointmentCalendarApp.getLangSelected())){
                notificationsUpdaterExecutor.shutdown();
                writeToLog("User Logout: " + loggedInUser.getUserName() + "(" + loggedInUser.getUserId() + ")");
                Platform.exit();
            }
        });
    }

    /**
     * Returns the AppointmentCalendarApp that is the parent of this controller.
     * @return
     */
    public AppointmentCalendar getAppointmentCalendarApp() {
        return myAppointmentCalendarApp;
    }

    /**
     * Sets the AppointmentCalendarApp that is the parent of this controller.
     * @param myAppointmentCalendarApp
     */
    public void setAppointmentCalendarApp(AppointmentCalendar myAppointmentCalendarApp) {
        this.myAppointmentCalendarApp = myAppointmentCalendarApp;
        databaseManager = myAppointmentCalendarApp.getDatabaseManager();
    }

    /**
     * Returns the logged in user.
     * @return
     */
    public User getLoggedInUser() {
        return loggedInUser;
    }

    /**
     * Sets the logged in user.
     * @param loggedInUser
     */
    public void setLoggedInUser(User loggedInUser) {
        this.loggedInUser = loggedInUser;
        mnuUser.setText(loggedInUser.getUserName());
        writeToLog("User Login: " + loggedInUser.getUserName() + "(" + loggedInUser.getUserId() + ")");
    }
    
    /**
     * Appends a line of text to the log file with a timestamp.
     * @param line
     */
    public void writeToLog(String line){
        try {
            Path logfile = Paths.get("log.txt");
            if(Files.notExists(logfile)){
                Files.createFile(logfile);
            }
            line = "\n" + LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME) + " - " + line;
            Files.write(logfile, line.getBytes(), StandardOpenOption.APPEND);
        }catch (IOException e) {
            System.out.println("Unable to open log file!");
        }
    }

    @Override
    public void changed(ObservableValue observable, Object oldValue, Object newValue) {
        updateCustomerDisplay();
    }
    
    /**
     * Shows the selected customers' information in the WebView on the Customers tab.
     */
    public void updateCustomerDisplay(){
        String html = "<!DOCTYPE html>\n" +
            "<html>\n" +
            "    <head>\n" +
            "        <title>" + selectedLang.getString("customerView") + "</title>\n" +
            "        <meta charset=\"UTF-8\">\n" +
            "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
            "    </head>\n" +
            "    <body>";
        String template = "<div>\n" +
            "    <h1>$customerName</h1>\n" +
            "    <p><big><strong>$phone</strong></big></p>\n" +
            "    <p>\n" +
            "        $address<br>\n" +
            "        $address2<br>\n" +
            "        $city, $country, $postalCode\n" +
            "    </p>\n" +
            "    <p><small><i>" + selectedLang.getString("createdByString") + "</i></small>\n" +
            "</div>";
        for(Customer customer : lstContacts.getSelectionModel().getSelectedItems()){
            String copy = new String(template);
            copy = copy.replace("$customerName", customer.getCustomerName());
            copy = copy.replace("$phone", customer.getAddress().getPhone());
            copy = copy.replace("$address2", customer.getAddress().getAddress2());
            copy = copy.replace("$address", customer.getAddress().getAddress());
            copy = copy.replace("$city", customer.getAddress().getCity().getCity());
            copy = copy.replace("$country", customer.getAddress().getCity().getCountry().getCountry());
            copy = copy.replace("$postalCode", customer.getAddress().getPostalCode());
            copy = copy.replace("$createdBy", customer.getCreatedBy());
            copy = copy.replace("$createDate", customer.getCreateDate().format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)));
            copy = copy.replace("$lastUpdateBy", customer.getLastUpdateBy());
            copy = copy.replace("$lastUpdate", customer.getLastUpdate().format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)));
            html += copy;
            html += "<hr>";
        }
        html += "    </body>\n</html>";
        webView.getEngine().loadContent(html);
    }

    /**
     * Returns the selected appointment.
     * @return
     */
    public Appointment getSelectedAppointment() {
        return selectedAppointment;
    }

    /**
     * Sets the selected appointment.
     * @param selectedAppointment
     */
    public void setSelectedAppointment(Appointment selectedAppointment) {
        this.selectedAppointment = selectedAppointment;
    }
    
    
    
    
    
}
