package appointmentcalendar;

import java.util.Optional;
import java.util.ResourceBundle;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * A convenience class for showing common dialogs.
 * @author Eric Boring <eboring@wgu.edu>
 */
public class DialogPopper {

    /**
     * Shows a dialog with Yes and No buttons.
     * @param title
     * @param header
     * @param content
     * @param owner
     * @param lang
     * @return
     */
    public static boolean showYesNo(String title, String header, String content, Stage owner, ResourceBundle lang){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        ButtonType btnYes = new ButtonType(lang != null ? lang.getString("yes") : "Yes");
        ButtonType btnNo = new ButtonType(lang != null ? lang.getString("no") : "No");
        alert.getButtonTypes().setAll(btnYes, btnNo);
        alert.initModality(Modality.APPLICATION_MODAL);
        alert.initOwner(owner);
        Optional<ButtonType> result = alert.showAndWait();
        return result.get() == btnYes;
    }
    
    /**
     * Shows and Error dialog with an Ok button.
     * @param title
     * @param header
     * @param content
     * @param owner
     * @param lang
     */
    public static void showError(String title, String header, String content, Stage owner, ResourceBundle lang){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        ButtonType btnOk = new ButtonType(lang != null ? lang.getString("ok") : "Ok");
        alert.getButtonTypes().setAll(btnOk);
        if(owner != null){
            alert.initModality(Modality.APPLICATION_MODAL);
            alert.initOwner(owner);
        }
        alert.showAndWait();
    }
}
