package appointmentcalendar;

import dbobjects.User;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.regex.Pattern;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 * Manages the edit use dialog box.
 * @author Eric Boring <eboring@wgu.edu>
 */
public class EditUserDialogController implements Initializable {
    private Stage stage;
    private User user;
    private MainWindowController mainWindowController;
    
    @FXML
    private Button btnCancel;

    @FXML
    private TextField txtUsername;

    @FXML
    private Button btnSave;

    @FXML
    private Label lblTitle;

    @FXML
    private PasswordField txtPassword2;

    @FXML
    private PasswordField txtPassword;
    
    @FXML
    private ImageView img8CharsYes;

    @FXML
    private ImageView img1DigitNo;

    @FXML
    private ImageView img1DigitYes;

    @FXML
    private ImageView img8CharsNo;

    @FXML
    private ImageView imgMixCaseNo;
    
    @FXML
    private ImageView imgMixCaseYes;

    @FXML
    private ImageView imgMatchesNo;

    @FXML
    private ImageView imgMatchesYes;

    @FXML
    void saveClicked(ActionEvent event) {
        user.setUserName(txtUsername.getText());
        if(validateInput()){
            user.setPassword(user.getPassword().setPasswordPlainText(txtPassword.getText()));
        }
        user.save();
        this.stage.close();
        mainWindowController.setLoggedInUser(user);
    }

    @FXML
    void cancelClicked(ActionEvent event) {
        this.stage.close();
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }  
    
    @FXML
    void textTyped(KeyEvent e){
        validateInput();
    }
    
    /**
     * Returns true if the user has entered a username and a valid password.
     * @return
     */
    public boolean validateInput(){
        boolean valid = true;
        if(txtPassword.getText().length() < 8){
            valid = false;
            img8CharsYes.setVisible(false);
            img8CharsNo.setVisible(true);
        }
        else{
            img8CharsYes.setVisible(true);
            img8CharsNo.setVisible(false);
        }
        if(Pattern.matches("\\d", txtPassword.getText())){
           img1DigitYes.setVisible(true);
           img1DigitNo.setVisible(false);
        }
        else{
            valid = false;
            img1DigitYes.setVisible(false);
            img1DigitNo.setVisible(true);
        }
        if(Pattern.matches("[a-z]", txtPassword.getText()) && Pattern.matches("[A-Z]", txtPassword.getText())){
            imgMixCaseYes.setVisible(true);
            imgMixCaseNo.setVisible(false);
        }
        else{
            valid = false;
            imgMixCaseYes.setVisible(false);
            imgMixCaseNo.setVisible(true);
        }
        if(txtPassword.getText().equals(txtPassword2.getText())){
            imgMatchesYes.setVisible(true);
            imgMatchesNo.setVisible(false);
        }
        else{
            valid = false;
            imgMatchesYes.setVisible(false);
            imgMatchesNo.setVisible(true);
        }
        if(txtUsername.getText().length() > 3 && txtPassword.getText().length() == 0 && txtPassword.getText().length() == 0){
            valid = true;
        }
        else if(txtUsername.getText().length() < 3){
            valid = false;
        }
        btnSave.setDisable(!valid);
        return valid;
    }

    /**
     * Returns the stage associated with this controller.
     * @return
     */
    public Stage getStage() {
        return stage;
    }

    /**
     * Sets the stage associated with this controller.
     * @param stage
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    /**
     * Returns the user being edited on this dialog.
     * @return
     */
    public User getUser() {
        return user;
    }

    /**
     * Sets the user being edited on this dialog.
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
        txtUsername.setText(user.getUserName());
    }

    /**
     * Returns the MainWindowController that is the parent of this controller.
     * @return
     */
    public MainWindowController getMainWindowController() {
        return mainWindowController;
    }

    /**
     * Sets the MainWindowController that is the parent of this controller.
     * @param mainWindowController
     */
    public void setMainWindowController(MainWindowController mainWindowController) {
        this.mainWindowController = mainWindowController;
    }
   
    
}
