package appointmentcalendar;

import dbobjects.Appointment;
import dbobjects.Customer;
import java.net.URL;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import javafx.util.StringConverter;

/**
 * FXML Controller class
 * Manages the dialog for editing appointments.
 * @author Eric Boring <eboring@wgu.edu>
 */
public class EditAppointmentDialogController implements Initializable {
    private Stage stage;
    private Appointment appointment;
    private MainWindowController mainWindowController;
    
    @FXML
    private Button btnStartPlusWeek;
    
    @FXML
    private Button btnStartMinusWeek;
    
    @FXML
    private Button btnEndPlusWeek;
    
    @FXML
    private Button btnEndMinusWeek;
    
    @FXML
    private Button btnStartZeroize;
    
    @FXML
    private Button btnStart30ize;
    
    @FXML
    private Button btnEndZeroize;
    
    @FXML
    private Button btnEnd30ize;
    
    @FXML
    private Button btnEndPlus15;

    @FXML
    private TextArea txtContact;

    @FXML
    private ComboBox<String> cmbType;

    @FXML
    private Button btnStartMinus15;

    @FXML
    private Button btnStartPlus30;

    @FXML
    private Button btnEndMinusDay;

    @FXML
    private Button btnEndPm;

    @FXML
    private Button btnStartPm;

    @FXML
    private Button btnEndMinusHour;

    @FXML
    private Button btnEndPlus30;

    @FXML
    private Button btnEndMinusYear;

    @FXML
    private Button btnStartMinus30;

    @FXML
    private Button btnEndPlusMonth;

    @FXML
    private ComboBox<LocalTime> cmbStartTime;

    @FXML
    private Button btnEndAm;

    @FXML
    private ComboBox<Customer> cmbCustomer;

    @FXML
    private DatePicker dtpStartDate;

    @FXML
    private ComboBox<LocalTime> cmbEndTime;

    @FXML
    private Button btnEndPlusYear;

    @FXML
    private TextField txtUrl;

    @FXML
    private Button btnStartPlusHour;

    @FXML
    private Button btnStartMinusMonth;

    @FXML
    private Button btnStartPlusDay;

    @FXML
    private TextArea txtDescription;

    @FXML
    private Button btnStartPlusMonth;

    @FXML
    private Button btnStartMinusYear;

    @FXML
    private Button btnEndMinus30;

    @FXML
    private Button btnCancel;

    @FXML
    private Button btnStartPlusYear;

    @FXML
    private Button btnSave;

    @FXML
    private DatePicker dtpEndDate;

    @FXML
    private Button btnEndMinusMonth;

    @FXML
    private Button btnEndMinus15;

    @FXML
    private Button btnStartAm;

    @FXML
    private Button btnStartMinusHour;

    @FXML
    private Button btnEndPlusDay;

    @FXML
    private Button btnEndPlusHour;

    @FXML
    private Label lblTitle;

    @FXML
    private Button btnStartMinusDay;

    @FXML
    private TextField txtTitle;

    @FXML
    private TextArea txtLocation;

    @FXML
    private Button btnStartPlus15;
    
    @FXML
    private Label lblDuration;
    
    @FXML
    private ImageView imgError;
    
    @FXML
    void saveClicked(ActionEvent event) {
        ArrayList<Appointment> overlaps = getOverlappingAppointments();
        if(overlaps.isEmpty()){
            appointment.setCustomer(cmbCustomer.getValue())
                    .setUser(mainWindowController.getLoggedInUser())
                    .setTitle(txtTitle.getText())
                    .setDescription(txtDescription.getText())
                    .setLocation(txtLocation.getText())
                    .setContact(txtContact.getText())
                    .setType(cmbType.getValue())
                    .setUrl(txtUrl.getText())
                    .setStart(LocalDateTime.of(dtpStartDate.getValue(), cmbStartTime.getValue()))
                    .setEnd(LocalDateTime.of(dtpEndDate.getValue(), cmbEndTime.getValue()))
                    .save();
            this.stage.close();
            mainWindowController.datePicked(null);
        }
        else{
            ResourceBundle lang =  mainWindowController.getAppointmentCalendarApp().getLangSelected();
            String appts = "";
            for(Appointment appt : overlaps){
                appts += "\u2022 " + appt.getStart().format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG, FormatStyle.SHORT)) + " " + lang.getString("to") + " " + appt.getEnd().format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG, FormatStyle.SHORT)) + " - " + appt.getTitle() + "\n";
            }
            DialogPopper.showError(lang.getString("apptitle"), lang.getString("overlapAppointmentDialog"), appts, stage, lang);
        }
    }

    @FXML
    void cancelClicked(ActionEvent event) {
        this.stage.close();
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        dtpStartDate.setValue(LocalDate.now());
        dtpEndDate.setValue(LocalDate.now());
        this.populateTime(cmbStartTime);
        this.populateTime(cmbEndTime);
        
        //Creating the actions for the convenience buttons that modify the
        //dates and times.
        //Lambda expressions make this concise and easier to read.
        btnStartMinusYear.setOnAction(e -> {
            dtpStartDate.setValue(dtpStartDate.getValue().minusYears(1));
            dtpEndDate.setValue(dtpEndDate.getValue().minusYears(1));
        });
        btnStartMinusMonth.setOnAction(e -> {
            dtpStartDate.setValue(dtpStartDate.getValue().minusMonths(1));
            dtpEndDate.setValue(dtpEndDate.getValue().minusMonths(1));
        });
        btnStartMinusWeek.setOnAction(e -> {
            dtpStartDate.setValue(dtpStartDate.getValue().minusDays(7));
            dtpEndDate.setValue(dtpEndDate.getValue().minusDays(7));
        });
        btnStartMinusDay.setOnAction(e -> {
            dtpStartDate.setValue(dtpStartDate.getValue().minusDays(1));
            dtpEndDate.setValue(dtpEndDate.getValue().minusDays(1));
        });
        btnStartPlusYear.setOnAction(e -> {
            dtpStartDate.setValue(dtpStartDate.getValue().plusYears(1));
            dtpEndDate.setValue(dtpEndDate.getValue().plusYears(1));
        });
        btnStartPlusMonth.setOnAction(e -> {
            dtpStartDate.setValue(dtpStartDate.getValue().plusMonths(1));
            dtpEndDate.setValue(dtpEndDate.getValue().plusMonths(1));
        });
        btnStartPlusWeek.setOnAction(e -> {
            dtpStartDate.setValue(dtpStartDate.getValue().plusDays(7));
            dtpEndDate.setValue(dtpEndDate.getValue().plusDays(7));
        });
        btnStartPlusDay.setOnAction(e -> {
            dtpStartDate.setValue(dtpStartDate.getValue().plusDays(1));
            dtpEndDate.setValue(dtpEndDate.getValue().plusDays(1));
        });

        btnStartMinusHour.setOnAction(e -> {
            cmbStartTime.setValue(cmbStartTime.getValue().minusHours(1));
            cmbEndTime.setValue(cmbEndTime.getValue().minusHours(1));
        });
        btnStartMinus30.setOnAction(e -> {
            cmbStartTime.setValue(cmbStartTime.getValue().minusMinutes(30));
            cmbEndTime.setValue(cmbEndTime.getValue().minusMinutes(30));
        });
        btnStartMinus15.setOnAction(e -> {
            cmbStartTime.setValue(cmbStartTime.getValue().minusMinutes(15));
            cmbEndTime.setValue(cmbEndTime.getValue().minusMinutes(15));
        });
        btnStartPlusHour.setOnAction(e -> {
            cmbStartTime.setValue(cmbStartTime.getValue().plusHours(1));
            cmbEndTime.setValue(cmbEndTime.getValue().plusHours(1));
        });
        btnStartPlus30.setOnAction(e -> {
            cmbStartTime.setValue(cmbStartTime.getValue().plusMinutes(30));
            cmbEndTime.setValue(cmbEndTime.getValue().plusMinutes(30));
        });
        btnStartPlus15.setOnAction(e -> {
            cmbStartTime.setValue(cmbStartTime.getValue().plusMinutes(15));
            cmbEndTime.setValue(cmbEndTime.getValue().plusMinutes(15));
        });
        
        btnStartAm.setOnAction(e -> {
            Duration d = Duration.between(cmbStartTime.getValue(), cmbEndTime.getValue());
            LocalTime t = cmbStartTime.getValue();
            if(t.getHour() > 11){
                cmbStartTime.setValue(t.minusHours(12));
            }
            cmbEndTime.setValue(cmbStartTime.getValue().plus(d));
        });
        btnStartPm.setOnAction(e -> {
            Duration d = Duration.between(cmbStartTime.getValue(), cmbEndTime.getValue());
            LocalTime t = cmbStartTime.getValue();
            if(t.getHour() < 12){
                cmbStartTime.setValue(t.plusHours(12));
            }
            cmbEndTime.setValue(cmbStartTime.getValue().plus(d));
        });
        btnStartZeroize.setOnAction(e -> {
            Duration d = Duration.between(cmbStartTime.getValue(), cmbEndTime.getValue());
            cmbStartTime.setValue(cmbStartTime.getValue().withMinute(0));
            cmbEndTime.setValue(cmbStartTime.getValue().plus(d));
        });
        btnStart30ize.setOnAction(e -> {
            Duration d = Duration.between(cmbStartTime.getValue(), cmbEndTime.getValue());
            cmbStartTime.setValue(cmbStartTime.getValue().withMinute(30));
            cmbEndTime.setValue(cmbStartTime.getValue().plus(d));
        });
        
        btnEndMinusYear.setOnAction(e -> dtpEndDate.setValue(dtpEndDate.getValue().minusYears(1)));
        btnEndMinusMonth.setOnAction(e -> dtpEndDate.setValue(dtpEndDate.getValue().minusMonths(1)));
        btnEndMinusWeek.setOnAction(e -> dtpEndDate.setValue(dtpEndDate.getValue().minusDays(7)));
        btnEndMinusDay.setOnAction(e -> dtpEndDate.setValue(dtpEndDate.getValue().minusDays(1)));
        btnEndPlusYear.setOnAction(e -> dtpEndDate.setValue(dtpEndDate.getValue().plusYears(1)));
        btnEndPlusMonth.setOnAction(e -> dtpEndDate.setValue(dtpEndDate.getValue().plusMonths(1)));
        btnEndPlusWeek.setOnAction(e -> dtpEndDate.setValue(dtpEndDate.getValue().plusDays(7)));
        btnEndPlusDay.setOnAction(e -> dtpEndDate.setValue(dtpEndDate.getValue().plusDays(1)));
        
        btnEndMinusHour.setOnAction(e -> cmbEndTime.setValue(cmbEndTime.getValue().minusHours(1)));
        btnEndMinus30.setOnAction(e -> cmbEndTime.setValue(cmbEndTime.getValue().minusMinutes(30)));
        btnEndMinus15.setOnAction(e -> cmbEndTime.setValue(cmbEndTime.getValue().minusMinutes(15)));
        btnEndPlusHour.setOnAction(e -> cmbEndTime.setValue(cmbEndTime.getValue().plusHours(1)));
        btnEndPlus30.setOnAction(e -> cmbEndTime.setValue(cmbEndTime.getValue().plusMinutes(30)));
        btnEndPlus15.setOnAction(e -> cmbEndTime.setValue(cmbEndTime.getValue().plusMinutes(15)));
        
        btnEndAm.setOnAction(e -> {
            LocalTime t = cmbEndTime.getValue();
            if(t.getHour() > 11){
                cmbEndTime.setValue(t.minusHours(12));
            }
        });
        btnEndPm.setOnAction(e -> {
            LocalTime t = cmbEndTime.getValue();
            if(t.getHour() < 12){
                cmbEndTime.setValue(t.plusHours(12));
            }
        });
        
        btnEndZeroize.setOnAction(e -> cmbEndTime.setValue(cmbEndTime.getValue().withMinute(0)));
        btnEnd30ize.setOnAction(e -> cmbEndTime.setValue(cmbEndTime.getValue().withMinute(30)));
        
        //This StringConverter class attempts to convert what the user typed to
        //a LocalTime object.
        cmbStartTime.setConverter(new StringConverter<LocalTime>() {
            @Override
            public String toString(LocalTime time) {
                return time.format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT));
            }
            @Override
            public LocalTime fromString(String string) {
                try{
                    Pattern r = Pattern.compile("(\\d{1,2}):(\\d{1,2})(pm|am)");
                    Matcher m = r.matcher(string.trim().replace(" ", "").toLowerCase());
                    if(m.find()){
                        int hour = Integer.parseInt(m.group(1));
                        int minute = Integer.parseInt(m.group(2));
                        String meridiem = m.group(3);
                        if(meridiem.equals("pm") && hour < 12){
                            hour = hour + 12;
                        }
                        else if(meridiem.equals("am") && hour == 12){
                            hour = 0;
                        }
                        string = (Integer.toString(hour).length() == 1 ? "0" : "") + hour + ":" + minute;
                    }
                    return LocalTime.parse(string);
                }
                catch(Exception e){
                    return LocalTime.now();
                }
            }
        });
        
        //This StringConverter class attempts to convert what the user typed to
        //a LocalTime object.
        cmbEndTime.setConverter(new StringConverter<LocalTime>() {
            @Override
            public String toString(LocalTime time) {
                return time.format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT));
            }
            @Override
            public LocalTime fromString(String string) {
                try{
                    Pattern r = Pattern.compile("(\\d{1,2}):(\\d{1,2})(pm|am)");
                    Matcher m = r.matcher(string.trim().replace(" ", "").toLowerCase());
                    if(m.find()){
                        int hour = Integer.parseInt(m.group(1));
                        int minute = Integer.parseInt(m.group(2));
                        String meridiem = m.group(3);
                        if(meridiem.equals("pm") && hour < 12){
                            hour = hour + 12;
                        }
                        else if(meridiem.equals("am") && hour == 12){
                            hour = 0;
                        }
                        string = (Integer.toString(hour).length() == 1 ? "0" : "") + hour + ":" + minute;
                    }
                    return LocalTime.parse(string);
                }
                catch(Exception e){
                    return LocalTime.now();
                }
            }
        });
        
        dtpStartDate.setOnAction(e -> validateDateTimes());
        dtpEndDate.setOnAction(e -> validateDateTimes());
        cmbStartTime.setOnAction(e -> validateDateTimes());
        cmbEndTime.setOnAction(e -> validateDateTimes());
        
        fixTextAreaFocusTraversal(txtContact);
        fixTextAreaFocusTraversal(txtDescription);
        fixTextAreaFocusTraversal(txtLocation);
    }  

    /**
     * The default behavior for pressing the TAB key inside a TextArea is to insert
     * a TAB character. In the use case here, we want the TAB key to go to the next
     * control instead. This method changes the key combination effects as follows:
     * TAB              Go to next control
     * Shift+TAB        Go to previous control
     * Ctrl+TAB         Insert TAB character
     * Ctrl+Shift+TAB   Insert BACKSPACE character (should be UNINDENT but Java doesn't appear to support this directly).
     * @param textArea 
     */
    private void fixTextAreaFocusTraversal(TextArea textArea){
        final KeyCombination tab = new KeyCodeCombination(KeyCode.TAB);
        final KeyCombination shiftTab = new KeyCodeCombination(KeyCode.TAB, KeyCombination.SHIFT_DOWN);
        final KeyCombination ctrlTab = new KeyCodeCombination(KeyCode.TAB, KeyCombination.CONTROL_DOWN);
        final KeyCombination ctrlShiftTab = new KeyCodeCombination(KeyCode.TAB, KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN);
        
        textArea.addEventFilter(KeyEvent.KEY_PRESSED, e -> {
            if(tab.match(e)){
                Node src = (Node)e.getSource();
                Parent parent = src.getParent();
                if(parent != null){
                    int index = parent.getChildrenUnmodifiable().indexOf(src);
                    Node next;
                    do{
                        index++;
                        if(index >= parent.getChildrenUnmodifiable().size())index = 0;
                        next = parent.getChildrenUnmodifiable().get(index);
                    }while(!next.isFocusTraversable());
                    next.requestFocus();
                    e.consume();
                }
            }
            else if(shiftTab.match(e)){
                Node src = (Node)e.getSource();
                Parent parent = src.getParent();
                if(parent != null){
                    int index = parent.getChildrenUnmodifiable().indexOf(src);
                    Node prev;
                    do{
                        index--;
                        if(index <= 0)index = parent.getChildrenUnmodifiable().size() - 1;
                        prev = parent.getChildrenUnmodifiable().get(index);
                    }while(!prev.isFocusTraversable());
                    prev.requestFocus();
                    e.consume();
                }
            }
            else if(ctrlTab.match(e)){
                Object obj = e.getSource();
                if(obj instanceof TextArea){
                    TextArea ta = (TextArea)obj;
                    ta.replaceSelection("\t");
                }
            }
            else if(ctrlTab.match(e)){
                Object obj = e.getSource();
                if(obj instanceof TextArea){
                    TextArea ta = (TextArea)obj;
                    ta.replaceSelection("\b");
                }
            }
        });
    }
    
    /**
     * This method updates the duration shown to the user and also warns if
     * the selected date/times are outside of business hours or overlap with
     * another appointment.
     */
    private void validateDateTimes(){
        Duration d = Duration.between(LocalDateTime.of(dtpStartDate.getValue(), cmbStartTime.getValue()), LocalDateTime.of(dtpEndDate.getValue(), cmbEndTime.getValue()));
        String s = "";
        String comma = " - ";
        if(d.toMinutes() == 0){
            s = mainWindowController.getAppointmentCalendarApp().getLangSelected().getString("noDuration");
        }
        else{
            s += getDurationString(d);
        }
        boolean outOfHours = isOutsideBusinessHours();
        if(outOfHours){
            s += " - " + mainWindowController.getAppointmentCalendarApp().getLangSelected().getString("outsideBusinessHours");
            comma = ", ";
        }
        boolean overlaps = !getOverlappingAppointments().isEmpty();
        if(overlaps){
            s += comma + mainWindowController.getAppointmentCalendarApp().getLangSelected().getString("overlapAppointment");
        }
        lblDuration.setText(s);
        imgError.setVisible(d.isNegative() || outOfHours || overlaps);
        btnSave.setDisable(d.isNegative() || outOfHours);
    }
    
    /**
     * Returns an ArrayList of appointments that overlap the selected 
     * dates/times.
     * @return
     */
    public ArrayList<Appointment> getOverlappingAppointments(){
        ArrayList<Appointment> overlaps = new ArrayList<>();
        if(mainWindowController != null){
            DatabaseManager databaseManager = this.mainWindowController.getAppointmentCalendarApp().getDatabaseManager();
            LocalDateTime start = LocalDateTime.of(dtpStartDate.getValue(), cmbStartTime.getValue());
            LocalDateTime end = LocalDateTime.of(dtpEndDate.getValue(), cmbEndTime.getValue());
            ArrayList<Appointment> appointmentsStart = databaseManager.getAppointmentsOnDate(start.toLocalDate());
            ArrayList<Appointment> appointmentsEnd = databaseManager.getAppointmentsOnDate(end.toLocalDate());
            for(Appointment appt : appointmentsStart){
                if(!appointmentsEnd.contains(appt))appointmentsEnd.add(appt); //Could do addAll() but this way avoids duplicates. ArrayList.contains() uses Object.equals() for comparison. dbobjects.Appointment overrides equals so this works even when the objects are separate instances.
            }
            for(Appointment appt : appointmentsEnd){
                if(start.isAfter(appt.getStart().minusMinutes(1)) && start.isBefore(appt.getEnd().plusMinutes(1)))overlaps.add(appt);
                else if(end.isAfter(appt.getStart().minusMinutes(1)) && end.isBefore(appt.getEnd().plusMinutes(1)))overlaps.add(appt);
                else if(appt.getStart().isAfter(start.minusMinutes(1)) && appt.getStart().isBefore(end.plusMinutes(1)))overlaps.add(appt);
                else if(appt.getEnd().isAfter(start.minusMinutes(1)) && appt.getEnd().isBefore(end.plusMinutes(1)))overlaps.add(appt);
            }
        }
        return overlaps;
    }
    
    /**
     * Returns true if the selected dates/times are outside of business hours.
     * @return
     */
    public boolean isOutsideBusinessHours(){
        if(this.mainWindowController != null){
            DatabaseManager databaseManager = this.mainWindowController.getAppointmentCalendarApp().getDatabaseManager();
            LocalTime startOpen = databaseManager.getOpenTime(dtpStartDate.getValue().getDayOfWeek());
            LocalTime startClose = databaseManager.getCloseTime(dtpStartDate.getValue().getDayOfWeek());
            LocalTime endOpen = databaseManager.getOpenTime(dtpEndDate.getValue().getDayOfWeek());
            LocalTime endClose = databaseManager.getCloseTime(dtpEndDate.getValue().getDayOfWeek());
            Duration startDayDuration = Duration.between(startOpen, startClose);

            LocalDateTime start = LocalDateTime.of(dtpStartDate.getValue(), cmbStartTime.getValue());
            LocalDateTime end = LocalDateTime.of(dtpEndDate.getValue(), cmbEndTime.getValue());
            Duration apptDuration = Duration.between(start, end);

            if(apptDuration.compareTo(startDayDuration) > 0)return true;
            if(start.toLocalTime().isBefore(startOpen) || start.toLocalTime().isAfter(startClose))return true;
            if(end.toLocalTime().isBefore(endOpen) || end.toLocalTime().isAfter(endClose))return true;
        }
        return false;
    }
    
    /**
     * Turns the supplied duration into a human readable string.
     * @param d
     * @return
     */
    public String getDurationString(Duration d){
        try{
            if(Math.abs(d.getSeconds()) < 60){
                return mainWindowController.getAppointmentCalendarApp().getLangSelected().getString("lessThanAMinute");
            }
            else if(Math.abs(d.toMinutes()) < 60){
                return d.toMinutes() + " " + mainWindowController.getAppointmentCalendarApp().getLangSelected().getString("minutes");
            }
            else if(Math.abs(d.toHours()) < 24){
                long remainingMinutes = d.minusHours(d.toHours()).toMinutes();
                if(remainingMinutes == 0){
                    return d.toHours() + " " + mainWindowController.getAppointmentCalendarApp().getLangSelected().getString("hours");
                }
                else{
                    return d.toHours() + " " + mainWindowController.getAppointmentCalendarApp().getLangSelected().getString("hours") + ", " + remainingMinutes + " " + mainWindowController.getAppointmentCalendarApp().getLangSelected().getString("minutes");
                }
            }
            else{
                Duration remHours = d.minusDays(d.toDays());
                long remainingHours = remHours.toHours();
                long remainingMinutes = remHours.minusHours(remHours.toHours()).toMinutes();
                String str = d.toDays() + " " + mainWindowController.getAppointmentCalendarApp().getLangSelected().getString("days");
                if(Math.abs(remainingHours) > 0){
                    str += ", " + remainingHours + " " + mainWindowController.getAppointmentCalendarApp().getLangSelected().getString("hours");
                }
                if(Math.abs(remainingMinutes) > 0){
                    str += ", " + remainingMinutes + " " + mainWindowController.getAppointmentCalendarApp().getLangSelected().getString("minutes");
                }
                return str;
            }
        }
        catch(NullPointerException e){
            return "";
        }
    }
    
    /**
     * Returns the stage associated with this controller.
     * @return
     */
    public Stage getStage() {
        return stage;
    }

    /**
     * Sets the stage associated with this controller.
     * @param stage
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    /**
     * Returns the appointment being edited by the shown dialog.
     * @return
     */
    public Appointment getAppointment() {
        return appointment;
    }

    /**
     * Sets the appointment being edited by the shown dialog.
     * @param appointment
     */
    public void setAppointment(Appointment appointment) {
        this.appointment = appointment;
        txtTitle.setText(appointment.getTitle());
        cmbCustomer.setValue(appointment.getCustomer());
        txtDescription.setText(appointment.getDescription());
        txtLocation.setText(appointment.getLocation());
        txtContact.setText(appointment.getContact());
        cmbType.setValue(appointment.getType());
        txtUrl.setText(appointment.getUrl());
        dtpStartDate.setValue(appointment.getStart() == null ? LocalDate.now() : appointment.getStart().toLocalDate());
        cmbStartTime.setValue(appointment.getStart() == null ? LocalTime.now() : appointment.getStart().toLocalTime());
        dtpEndDate.setValue(appointment.getEnd() == null ? LocalDate.now() : appointment.getEnd().toLocalDate());
        cmbEndTime.setValue(appointment.getEnd() == null ? LocalTime.now() : appointment.getEnd().toLocalTime());
    }

    /**
     * Returns the MainWindowController that is the parent of this controller.
     * @return
     */
    public MainWindowController getMainWindowController() {
        return mainWindowController;
    }

    /**
     * Sets the MainWindowController that is the parent of this controller.
     * @param mainWindowController
     */
    public void setMainWindowController(MainWindowController mainWindowController) {
        this.mainWindowController = mainWindowController;
        validateDateTimes();
    }

    /**
     * Sets the list of customers to use for the customers dropdown box.
     * @param customers
     */
    public void setCustomersList(ObservableList<Customer> customers) {
        cmbCustomer.setItems(customers);
    }
    
    /**
     * Sets the list of types to use for the types dropdown box.
     * @param types
     */
    public void setTypeList(ObservableList<String> types){
        cmbType.setItems(types);
    }
    
    /**
     * Fills the combobox with 24 hours of times in 30 minute increments.
     * E.g. 12:00 AM, 12:30 AM, 1:00 AM, 1:30 AM, etc.
     * @param combobox 
     */
    private void populateTime(ComboBox<LocalTime> combobox){
        LocalTime time = LocalTime.MIDNIGHT;
        ObservableList<LocalTime> timeList = FXCollections.observableArrayList();
        for(int i = 0; i < 48; i++){
            timeList.add(time);
            time = time.plusMinutes(30);            
        }
        combobox.setItems(timeList);
        combobox.getSelectionModel().selectFirst();
    }
    
}
