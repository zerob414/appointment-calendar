package appointmentcalendar;

import dbobjects.Appointment;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

/**
 * FXML Controller class
 * Controller for the week view of the application.
 * @author Eric Boring <eboring@wgu.edu>
 */
public class WeekViewController implements Initializable {
    private MainWindowController mainWindowController;
    private LocalDate date;
    private Scene scene;
    
    @FXML
    private GridPane grid;

    @FXML
    private Label lblTitle;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        LocalTime time = LocalTime.MIDNIGHT;
        for(int i = 0; i < 48; i += 2){
            Label lblTime = new Label(time.format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT)));
            HBox hb = new HBox();
            hb.setAlignment(Pos.TOP_CENTER);
            hb.getChildren().add(lblTime);
            grid.add(hb, 0, i+1, 1, 2);
            lblTime.setPadding(new Insets(5.0));
            String color = time.getHour() % 2 == 1 ? "azure;" : "white;";
            hb.setStyle("-fx-background-color: " + color);
            time = time.plusHours(1);
        }
    }    

    /**
     * Returns the date this week view is centered around.
     * @return
     */
    public LocalDate getDate() {
        return date;
    }

    /**
     * Sets the date this week view is centered around.
     * @param date
     */
    public void setDate(LocalDate date) {
        this.date = date;
        int dayOfWeek = date.getDayOfWeek().getValue();
        dayOfWeek = dayOfWeek == 7 ? 0 : dayOfWeek;
        LocalDate firstDay = date.minusDays(dayOfWeek);
        LocalDate lastDay = firstDay.plusDays(6);
        lblTitle.setText(firstDay.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)) + " to " + lastDay.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)));
        clearAppointmentsColumns();
        for(int i = 0; i < 7; i++){
            refreshDay(i + 1, firstDay.plusDays(i));
        }
    }
    
    /**
     * Loads all of the appointments for the specified date and adds them to the
     * day column indicated.
     * @param day
     * @param date 
     */
    private void refreshDay(int day, LocalDate date){
        if(this.mainWindowController != null && day > 0 && day < 8){
            DatabaseManager databaseManager = this.mainWindowController.getAppointmentCalendarApp().getDatabaseManager();
            ArrayList<Appointment> appointments = databaseManager.getAppointmentsOnDate(date);
            
            for(Appointment appt : appointments){
                int start = appt.getStart().getHour() * 2;
                int offset = appt.getStart().getMinute() > 29 ? 1 : 0;
                int vSpan = (int)Math.round(appt.getDuration().toMinutes() / 30.0);
                vSpan = vSpan > 0 ? vSpan : 1;
                AppointmentHyperlink apptLink = new AppointmentHyperlink(appt, AppointmentHyperlink.Size.MEDIUM);
                apptLink.setOnAction(e -> {
                    AppointmentHyperlink l = (AppointmentHyperlink)e.getSource();
                    l.setStyle("-fx-font-weight: bold; -fx-background-color: yellow;");
                    this.mainWindowController.updateInfoPanel(l.getAppointment(), date);
                    this.mainWindowController.setSelectedAppointment(l.getAppointment());
                });
                FlowPane fp = new FlowPane();
                fp.setStyle("-fx-background-color: #ffcccc; -fx-background-radius: 10;");
                fp.getChildren().add(apptLink);
                grid.add(fp, day, start + offset + 1, 1, vSpan);
            }
            
            for(int i = 0; i < 48; i++){
                Node node = getNodeFromGridPane(grid, day, i + 1);
                if(node == null){
                    FlowPane fp = new FlowPane();
                    String color = i / 2  % 2 == 1 ? "azure;" : "white;";
                    fp.setStyle("-fx-background-color: " + color);
                    grid.add(fp, day, i + 1);
                    fp.toBack();
                }
            }
        }
    }
    
    private void clearAppointmentsColumns(){
        ArrayList<Node> removeList = new ArrayList<>();
        for(Node node : grid.getChildrenUnmodifiable()){
            Integer row = GridPane.getRowIndex(node);
            Integer col = GridPane.getColumnIndex(node);
            row = row == null ? 0 : row;
            col = col == null ? 0 : col;
            if(row > 0 && col > 0){
                removeList.add(node);
            }
        }
        grid.getChildren().removeAll(removeList);
    }
    
    /**
     * Returns the MainWindowController that is the parent of this controller.
     * @return
     */
    public MainWindowController getMainWindowController() {
        return mainWindowController;
    }

    /**
     * Sets the MainWindowController that is the parent of this controller.
     * @param mainWindowController
     */
    public void setMainWindowController(MainWindowController mainWindowController) {
        this.mainWindowController = mainWindowController;
    }
    
    /**
     * Returns the node located at the specified coordinates in the GridPane.
     * Borrowed from Shreyas Dave at: https://stackoverflow.com/questions/20655024/javafx-gridpane-retrieve-specific-cell-content
     * @param gridPane
     * @param col
     * @param row
     * @return 
     */
    private Node getNodeFromGridPane(GridPane gridPane, int col, int row) {
        for (Node node : gridPane.getChildren()) {
            Integer c = GridPane.getColumnIndex(node);
            c = c == null ? 0 : c;
            Integer r = GridPane.getRowIndex(node);
            r = r == null ? 0 : r;
            if (c == col && r  == row) {
                return node;
            }
        }
        return null;
    }

    /**
     * Returns the scene associated with this controller.
     * @return
     */
    public Scene getScene() {
        return scene;
    }

    /**
     * Sets the scene associated with this controller.
     * @param scene
     */
    public void setScene(Scene scene) {
        this.scene = scene;
    }
    
}
