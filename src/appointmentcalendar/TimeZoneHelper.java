package appointmentcalendar;

import java.time.ZoneId;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * A helper class to make listing TimeZones easier.
 * @author Eric Boring <eboring@wgu.edu>
 */
public final class TimeZoneHelper {
    private String id;
    private String region;
    private String name;

    /**
     * Creates a new TimeZoneHelper with the ID.
     * @param id
     */
    public TimeZoneHelper(String id) {
        this.setId(id);
    }
    
    /**
     * Returns the ID of the TimeZone this object represents.
     * @return
     */
    public String getId() {
        return id;
    }

    /**
     * Sets  the ID of the TimeZone this object represents.
     * @param id
     */
    public void setId(String id) {
        this.id = id;
        if(id.contains("/")){
            this.region = id.split("/")[0];
            this.name = id.split("/")[1];
        }
        else{
            name = id;
        }
    }

    /**
     * Returns the region portion of the this TimeZone's ID.
     * @return
     */
    public String getRegion() {
        return region;
    }

    /**
     * Sets the region portion of the this TimeZone's ID.
     * @param region
     */
    public void setRegion(String region) {
        this.region = region;
    }

    /**
     * Returns the name portion of the this TimeZone's ID.
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name portion of the this TimeZone's ID.
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * Returns true if this TimeZone has a region.
     * @return
     */
    public boolean hasRegion(){
        return region != null;
    }
    
    /**
     * Returns a ZoneId object for this TimeZone.
     * @return
     */
    public ZoneId getZoneId(){
        return TimeZone.getTimeZone(id).toZoneId();
    }
    
    /**
     * Returns a TimeZone object for this TimeZone.
     * @return
     */
    public TimeZone getTimeZone(){
        return TimeZone.getTimeZone(id);
    }
    
    /**
     * Returns a string representing the offset from UTC for this TimeZone.
     * Borrowed from Mkyong with some modifications: https://www.mkyong.com/java/java-display-list-of-timezone-with-gmt/
     * @return 
     */
    public String getOffsetString(){
        TimeZone tz = TimeZone.getTimeZone(id);
        long hours = TimeUnit.MILLISECONDS.toHours(tz.getRawOffset());
        long minutes = TimeUnit.MILLISECONDS.toMinutes(tz.getRawOffset()) 
                          - TimeUnit.HOURS.toMinutes(hours);
        // avoid -4:-30 issue
        minutes = Math.abs(minutes);

        String result = "";
        if (hours > 0) {
                result = String.format("(GMT+%d:%02d)", hours, minutes);
        } else {
                result = String.format("(GMT%d:%02d)", hours, minutes);
        }
        return result; 
    }
}
